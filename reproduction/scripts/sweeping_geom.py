
from netgen.meshing import *
from netgen.csg import *
import ngsolve
from math import pi,sin,cos
import copy
from netgen.geom2d import SplineGeometry

def Layered_Mesh(r_layer,nx,quads,refine_layer,theta_bnd,x_max=pi,mapping=None):


    vertices_layer_nr = {}
    vertices_layer_coord = {}
    mesh_nodes = {}

    mesh = Mesh()
    mesh.dim=2

    mesh.Add(FaceDescriptor(surfnr=1,domin=1,bc=1))

    n_layers = len(r_layer) - 1

    slavej = []
    masterj = []

    pids_inner = []
    coords_inner = []
    for j in range(nx+1):
        x_inner,y_inner = x_max*j/nx, r_layer[0]
        if mapping: 
            x_inner,y_inner = mapping(x_inner,y_inner)
        coords_inner.append( (x_inner,y_inner))
        pids_inner.append(mesh.Add (MeshPoint(Pnt(x_inner,y_inner,0))))
        if j == 0:
            slavej.append(pids_inner[-1])
        if j == nx:
            masterj.append(pids_inner[-1])
        
    vertices_layer_nr[n_layers] = pids_inner
    vertices_layer_coord[n_layers] = coords_inner


    def MakeLayer(quads, nx,mesh,pids_next_lvl,r_start,r_end,mapping = None,refine_layer=False,domin=1,bc=1):

        fd_layer = mesh.Add(FaceDescriptor(surfnr=1,domin=domin,bc=1))

        pids_start = pids_next_lvl
        pids_middle = []
        pids_end = []
        if refine_layer:
            pids_refined_center = []
            pids_refined_end = []
        r_middle = r_start + (r_end-r_start)/2
        for j in range(nx+1):
            x_middle,y_middle = x_max*j/nx, r_middle
            x_end,y_end = x_max*j/nx, r_end
            if mapping:
                x_middle,y_middle = mapping(x_middle,y_middle)
                x_end,y_end = mapping(x_end,y_end)  
                
            node_nr_middle = mesh.Add (MeshPoint(Pnt(x_middle,y_middle,0)))
            pids_middle.append(node_nr_middle)    
            node_nr_end = mesh.Add (MeshPoint(Pnt(x_end,y_end,0)))
            pids_end.append(node_nr_end)  

            if j == 0:
                slavej.append(pids_middle[-1])
                slavej.append(pids_end[-1])
            if j == nx:
                masterj.append(pids_middle[-1])
                masterj.append(pids_end[-1])

            mesh_nodes[node_nr_middle.nr] = (x_middle,y_middle)
            mesh_nodes[node_nr_end.nr] = (x_end,y_end)

        if quads:
            for i in range(nx):
                elpids1 = [pids_start[i],pids_start[i+1],pids_middle[i+1],pids_middle[i] ]
                elpids2 = [pids_middle[i],pids_middle[i+1],pids_end[i+1],pids_end[i] ]
                mesh.Add(Element2D(fd_layer,elpids1)) 
                mesh.Add(Element2D(fd_layer,elpids2)) 
            for i in range(nx):
                mesh.Add(Element1D([pids_start[i], pids_start[i+1]], index=bc))
            mesh.SetBCName(bc-1,"layer"+str(domin)+"-bnd")
            return nx,pids_end

        if refine_layer:
            for j in range(nx):
                x_end_refined,y_end = x_max*j/nx + x_max*1/(2*nx),r_end
                x_center_refined,y_center_refined = x_max*j/nx + x_max*1/(2*nx) , r_middle + 0.5*(r_end-r_middle)
                if mapping:
                    x_end_refined,y_end = mapping(x_end_refined,y_end)
                    x_center_refined,y_center_refined = mapping(x_center_refined,y_center_refined)
                
                node_nr_end_refined = mesh.Add (MeshPoint(Pnt(x_end_refined,y_end,0)))
                pids_refined_end.append(node_nr_end_refined) 

                node_nr_center_refined = mesh.Add (MeshPoint(Pnt(x_center_refined,y_center_refined,0)))
                pids_refined_center.append(node_nr_center_refined) 

                mesh_nodes[node_nr_end_refined.nr] = (x_end_refined,y_end)
                mesh_nodes[node_nr_center_refined.nr]  = (x_center_refined,y_center_refined)
                                    
        
        for i in range(nx):


            elpids1 = [pids_start[i],pids_start[i+1],pids_middle[i] ]
            elpids2 = [pids_start[i+1],pids_middle[i+1],pids_middle[i] ]

            mesh.Add(Element2D(fd_layer,elpids1)) 
            mesh.Add(Element2D(fd_layer,elpids2))   

            if not refine_layer:
                elpids3 = [pids_middle[i],pids_middle[i+1],pids_end[i] ]
                elpids4 = [pids_middle[i+1],pids_end[i+1],pids_end[i] ]
                mesh.Add(Element2D(fd_layer,elpids3)) 
                mesh.Add(Element2D(fd_layer,elpids4))          
            else:
                elpids3 = [pids_middle[i],pids_middle[i+1],pids_refined_center[i] ]              
                elpids5 = [pids_middle[i+1],pids_end[i+1],pids_refined_center[i]]
                elpids7 = [pids_refined_center[i],pids_end[i+1],pids_refined_end[i]]

                elpids4 = [pids_middle[i],pids_refined_center[i],pids_end[i]]
                elpids6 = [pids_refined_center[i],pids_refined_end[i],pids_end[i]]
                
                mesh.Add(Element2D(fd_layer,elpids3)) 
                mesh.Add(Element2D(fd_layer,elpids4)) 
                mesh.Add(Element2D(fd_layer,elpids5)) 
                mesh.Add(Element2D(fd_layer,elpids6)) 
                mesh.Add(Element2D(fd_layer,elpids7)) 


        for i in range(nx):
            mesh.Add(Element1D([pids_start[i], pids_start[i+1]], index=bc))
            
        mesh.SetBCName(bc-1,"layer"+str(domin)+"-bnd")


        if refine_layer:
            pids_next_lvl = []
            for i in range(len(pids_end)):
                pids_next_lvl.append(pids_end[i])
                if i < len(pids_refined_end):
                    pids_next_lvl.append(pids_refined_end[i])
            return nx*2,pids_next_lvl
        else:
            return nx,pids_end


    pids_next_lvl = pids_inner

    for i in range(n_layers):

        #refine_layer = True if (i >= 4 and i <= 7) else False # Mesh-RR
        #refine_layer = True if (i >= 5 and i <= 8) else False # Mesh-R
        #refine_layer = False
        nx,pids_next_lvl = MakeLayer(quads=quads,nx=nx,mesh=mesh,pids_next_lvl=pids_next_lvl,r_start=r_layer[i],r_end=r_layer[i+1], 
                       mapping = None,
                       refine_layer=refine_layer[i],
                       domin = n_layers-i,
                       bc =  n_layers+1-i
              
                      )
    for i in range(nx):
        mesh.Add(Element1D([pids_next_lvl[i], pids_next_lvl[i+1]], index=1))

    if theta_bnd == 'periodic': 
        for j in range(len(slavej)):        
            mesh.AddPointIdentification(masterj[j],slavej[j],identnr=2,type=2) 
    else:
        for j in range(len(slavej)-1):  
            mesh.Add(Element1D([slavej[j], slavej[j+1]], index=n_layers+2))
            mesh.Add(Element1D([masterj[j], masterj[j+1]], index=n_layers+3))
            
        mesh.SetBCName(n_layers+1,"left")
        mesh.SetBCName(n_layers+2,"right")


    mesh.Compress()       
    mesh = ngsolve.Mesh(mesh)
    return mesh


def HybridLayered_Mesh(r_layer,nx,quads,refine_layer,theta_bnd,x_max=pi):


    vertices_layer_nr = {}
    vertices_layer_coord = {}
    mesh_nodes = {}

    mesh = Mesh()
    mesh.dim=2

    mesh.Add(FaceDescriptor(surfnr=1,domin=1,bc=1))

    n_layers = len(r_layer) - 1

    slavej = []
    masterj = []

    pids_inner = []
    coords_inner = []
    for j in range(nx+1):
        x_inner,y_inner = x_max*j/nx, r_layer[0]
        coords_inner.append( (x_inner,y_inner))
        pids_inner.append(mesh.Add (MeshPoint(Pnt(x_inner,y_inner,0))))
        if j == 0:
            slavej.append(pids_inner[-1])
        if j == nx:
            masterj.append(pids_inner[-1])
        
    vertices_layer_nr[n_layers] = pids_inner
    vertices_layer_coord[n_layers] = coords_inner


    def MakeLayer(quads, nx,mesh,pids_next_lvl,r_start,r_end,mapping = None,refine_layer=False,domin=1,bc=1):

        fd_layer = mesh.Add(FaceDescriptor(surfnr=1,domin=domin,bc=1))

        pids_start = pids_next_lvl
        pids_middle = []
        pids_end = []
        if refine_layer:
            pids_refined_center = []
            pids_refined_end = []
        r_middle = r_start + (r_end-r_start)/2
        for j in range(nx+1):
            x_middle,y_middle = x_max*j/nx, r_middle
            x_end,y_end = x_max*j/nx, r_end
            if mapping:
                x_middle,y_middle = mapping(x_middle,y_middle)
                x_end,y_end = mapping(x_end,y_end)  
                
            node_nr_middle = mesh.Add (MeshPoint(Pnt(x_middle,y_middle,0)))
            pids_middle.append(node_nr_middle)    
            node_nr_end = mesh.Add (MeshPoint(Pnt(x_end,y_end,0)))
            pids_end.append(node_nr_end)  

            if j == 0:
                slavej.append(pids_middle[-1])
                slavej.append(pids_end[-1])
            if j == nx:
                masterj.append(pids_middle[-1])
                masterj.append(pids_end[-1])

            mesh_nodes[node_nr_middle.nr] = (x_middle,y_middle)
            mesh_nodes[node_nr_end.nr] = (x_end,y_end)

        if quads:
            for i in range(nx):
                elpids1 = [pids_start[i],pids_start[i+1],pids_middle[i+1],pids_middle[i] ]
                elpids2 = [pids_middle[i],pids_middle[i+1],pids_end[i+1],pids_end[i] ]
                mesh.Add(Element2D(fd_layer,elpids1)) 
                mesh.Add(Element2D(fd_layer,elpids2)) 
            for i in range(nx):
                mesh.Add(Element1D([pids_start[i], pids_start[i+1]], index=bc))
            mesh.SetBCName(bc-1,"layer"+str(domin)+"-bnd")
            return nx,pids_end

        if refine_layer:
            for j in range(nx):
                x_end_refined,y_end = x_max*j/nx + x_max*1/(2*nx),r_end
                x_center_refined,y_center_refined = x_max*j/nx + x_max*1/(2*nx) , r_middle + 0.5*(r_end-r_middle)
                if mapping:
                    x_end_refined,y_end = mapping(x_end_refined,y_end)
                    x_center_refined,y_center_refined = mapping(x_center_refined,y_center_refined)
                
                node_nr_end_refined = mesh.Add (MeshPoint(Pnt(x_end_refined,y_end,0)))
                pids_refined_end.append(node_nr_end_refined) 

                node_nr_center_refined = mesh.Add (MeshPoint(Pnt(x_center_refined,y_center_refined,0)))
                pids_refined_center.append(node_nr_center_refined) 

                mesh_nodes[node_nr_end_refined.nr] = (x_end_refined,y_end)
                mesh_nodes[node_nr_center_refined.nr]  = (x_center_refined,y_center_refined)
                                    
        
        for i in range(nx):


            elpids1 = [pids_start[i],pids_start[i+1],pids_middle[i] ]
            elpids2 = [pids_start[i+1],pids_middle[i+1],pids_middle[i] ]

            mesh.Add(Element2D(fd_layer,elpids1)) 
            mesh.Add(Element2D(fd_layer,elpids2))   

            if not refine_layer:
                elpids3 = [pids_middle[i],pids_middle[i+1],pids_end[i] ]
                elpids4 = [pids_middle[i+1],pids_end[i+1],pids_end[i] ]
                mesh.Add(Element2D(fd_layer,elpids3)) 
                mesh.Add(Element2D(fd_layer,elpids4))          
            else:
                elpids3 = [pids_middle[i],pids_middle[i+1],pids_refined_center[i] ]              
                elpids5 = [pids_middle[i+1],pids_end[i+1],pids_refined_center[i]]
                elpids7 = [pids_refined_center[i],pids_end[i+1],pids_refined_end[i]]

                elpids4 = [pids_middle[i],pids_refined_center[i],pids_end[i]]
                elpids6 = [pids_refined_center[i],pids_refined_end[i],pids_end[i]]
                
                mesh.Add(Element2D(fd_layer,elpids3)) 
                mesh.Add(Element2D(fd_layer,elpids4)) 
                mesh.Add(Element2D(fd_layer,elpids5)) 
                mesh.Add(Element2D(fd_layer,elpids6)) 
                mesh.Add(Element2D(fd_layer,elpids7)) 


        for i in range(nx):
            mesh.Add(Element1D([pids_start[i], pids_start[i+1]], index=bc))
            
        mesh.SetBCName(bc-1,"layer"+str(domin)+"-bnd")

        if refine_layer:
            pids_next_lvl = []
            for i in range(len(pids_end)):
                pids_next_lvl.append(pids_end[i])
                if i < len(pids_refined_end):
                    pids_next_lvl.append(pids_refined_end[i])
            return nx*2,pids_next_lvl
        else:
            return nx,pids_end


    pids_next_lvl = pids_inner

    for i in range(n_layers):

        #refine_layer = True if (i >= 4 and i <= 7) else False # Mesh-RR
        #refine_layer = True if (i >= 5 and i <= 8) else False # Mesh-R
        #refine_layer = False
        nx,pids_next_lvl = MakeLayer(quads=quads[i],nx=nx,mesh=mesh,pids_next_lvl=pids_next_lvl,r_start=r_layer[i],r_end=r_layer[i+1], 
                       mapping = None,
                       refine_layer=refine_layer[i],
                       domin = n_layers-i,
                       bc =  n_layers+1-i
              
                      )
    for i in range(nx):
        mesh.Add(Element1D([pids_next_lvl[i], pids_next_lvl[i+1]], index=1))

    if theta_bnd == 'periodic': 
        for j in range(len(slavej)):        
            mesh.AddPointIdentification(masterj[j],slavej[j],identnr=2,type=2) 
    else:
        for j in range(len(slavej)-1):  
            mesh.Add(Element1D([slavej[j], slavej[j+1]], index=n_layers+2))
            mesh.Add(Element1D([masterj[j], masterj[j+1]], index=n_layers+3))
            
        mesh.SetBCName(n_layers+1,"left")
        mesh.SetBCName(n_layers+2,"right")


    mesh.Compress()       
    mesh = ngsolve.Mesh(mesh)
    return mesh


from netgen.meshing import  Mesh as NetMesh
from ngsolve import Mesh as NGSMesh

def Make1DMesh(layer_nr,r_layer):
    
    mesh_1D = NetMesh(dim=1)
    pids = []   

    for nr,r_cord in enumerate(r_layer[-1-layer_nr:]):
        pids.append (mesh_1D.Add (MeshPoint(Pnt(r_cord, 0, 0))))  
        if nr + 1 < len(r_layer[-1-layer_nr:]):
            r_mid = r_cord + 0.5*(r_layer[-1-layer_nr:][nr+1] - r_cord )
            pids.append (mesh_1D.Add (MeshPoint(Pnt(r_mid, 0, 0))))  

    n_mesh = len(pids)-1
    for i in range(n_mesh):
        mesh_1D.Add(Element1D([pids[i],pids[i+1]],index=1))
    mesh_1D.Add (Element0D( pids[0], index=1))
    mesh_1D.Add (Element0D( pids[n_mesh], index=2))
    mesh_1D.SetBCName(0,"left")
    mesh_1D.SetBCName(1,"right")
    mesh_1D = NGSMesh(mesh_1D)    
    return mesh_1D



def HybridLayered_Mesh_gen(r_layer,nx,quads,refine_layer,theta_bnd,x_max=pi,order_geom=5):

    mapping = lambda x,y : ( y*sin(x),
                             y*cos(x))
   
    #mapping = None
    vertices_layer_nr = {}
    vertices_layer_coord = {}
    mesh_nodes = {}

    mesh = Mesh()
    mesh.dim=2

    n_layers = len(r_layer) - 1

    slavej = []
    masterj = []
    
    pid_center = mesh.Add(MeshPoint(Pnt(0.0,0.0,0)))
    pids_inner = []
    coords_inner = []
    for j in range(nx):
        x_inner,y_inner = x_max*j/nx, r_layer[1]
        if mapping: 
            x_inner,y_inner = mapping(x_inner,y_inner)
        coords_inner.append( (x_inner,y_inner))
        node_nr = mesh.Add (MeshPoint(Pnt(x_inner,y_inner,0)))
        pids_inner.append(node_nr)
        #pids_inner.append(mesh.Add (MeshPoint(Pnt(x_inner,y_inner,0))))
        mesh_nodes[node_nr.nr] = (x_inner,y_inner)
        #if j == 0:
        #    slavej.append(pids_inner[-1])
        #if j == nx:
        #    masterj.append(pids_inner[-1])
    pids_inner.append(pids_inner[0])
    vertices_layer_nr[n_layers] = pids_inner
    vertices_layer_coord[n_layers] = coords_inner
    
    fd_layer = mesh.Add(FaceDescriptor(surfnr=1,domin=n_layers-1,bc=1))
    for i in range(nx):
        elpids_inner  = [pid_center,pids_inner[i+1],pids_inner[i] ]
        mesh.Add(Element2D(fd_layer,elpids_inner)) 


    def MakeLayer(quads, nx,mesh,pids_next_lvl,r_start,r_end,mapping = None,refine_layer=False,domin=1,bc=1):

        fd_layer = mesh.Add(FaceDescriptor(surfnr=1,domin=domin,bc=1))

        pids_start = pids_next_lvl
        pids_middle = []
        pids_end = []
        if refine_layer:
            pids_refined_center = []
            pids_refined_end = []
        r_middle = r_start + (r_end-r_start)/2
        for j in range(nx+1):
            x_middle,y_middle = x_max*j/nx, r_middle
            x_end,y_end = x_max*j/nx, r_end
            if mapping:
                x_middle,y_middle = mapping(x_middle,y_middle)
                x_end,y_end = mapping(x_end,y_end)  
            
            if j == nx:
                #node_nr_middle = mesh.Add (MeshPoint(Pnt(x_middle,y_middle,0)))
                #pids_middle.append(node_nr_middle)    
                #mesh_nodes[node_nr_middle.nr] = (x_middle,y_middle)
                #node_nr_end = mesh.Add (MeshPoint(Pnt(x_end,y_end,0)))
                pids_middle.append(pids_middle[0])   
                pids_end.append(pids_end[0])   
            else:
                node_nr_middle = mesh.Add (MeshPoint(Pnt(x_middle,y_middle,0)))
                pids_middle.append(node_nr_middle)    
                node_nr_end = mesh.Add (MeshPoint(Pnt(x_end,y_end,0)))
                pids_end.append(node_nr_end)  
                mesh_nodes[node_nr_middle.nr] = (x_middle,y_middle)
                mesh_nodes[node_nr_end.nr] = (x_end,y_end)

            #if j == 0:
            #    slavej.append(pids_middle[-1])
            #    slavej.append(pids_end[-1])
            #if j == nx:
            #    masterj.append(pids_middle[-1])
            #    masterj.append(pids_end[-1])


        if quads:
            for i in range(nx):
                elpids1 = [pids_start[i],pids_start[i+1],pids_middle[i+1],pids_middle[i] ]
                elpids2 = [pids_middle[i],pids_middle[i+1],pids_end[i+1],pids_end[i] ]
                mesh.Add(Element2D(fd_layer,elpids1)) 
                mesh.Add(Element2D(fd_layer,elpids2)) 
            for i in range(nx):
                mesh.Add(Element1D([pids_start[i], pids_start[i+1]], index=bc))
            mesh.SetBCName(bc-1,"layer"+str(domin+1)+"-bnd")
            return nx,pids_end

        if refine_layer:
            for j in range(nx):
                x_end_refined,y_end = x_max*j/nx + x_max*1/(2*nx),r_end
                x_center_refined,y_center_refined = x_max*j/nx + x_max*1/(2*nx) , r_middle + 0.5*(r_end-r_middle)
                if mapping:
                    x_end_refined,y_end = mapping(x_end_refined,y_end)
                    x_center_refined,y_center_refined = mapping(x_center_refined,y_center_refined)
                
                node_nr_end_refined = mesh.Add (MeshPoint(Pnt(x_end_refined,y_end,0)))
                pids_refined_end.append(node_nr_end_refined) 

                node_nr_center_refined = mesh.Add (MeshPoint(Pnt(x_center_refined,y_center_refined,0)))
                pids_refined_center.append(node_nr_center_refined) 

                mesh_nodes[node_nr_end_refined.nr] = (x_end_refined,y_end)
                mesh_nodes[node_nr_center_refined.nr]  = (x_center_refined,y_center_refined)
                                    
        
        for i in range(nx):


            elpids1 = [pids_start[i],pids_start[i+1],pids_middle[i] ]
            elpids2 = [pids_start[i+1],pids_middle[i+1],pids_middle[i] ]

            mesh.Add(Element2D(fd_layer,elpids1)) 
            mesh.Add(Element2D(fd_layer,elpids2))   

            if not refine_layer:
                elpids3 = [pids_middle[i],pids_middle[i+1],pids_end[i] ]
                elpids4 = [pids_middle[i+1],pids_end[i+1],pids_end[i] ]
                mesh.Add(Element2D(fd_layer,elpids3)) 
                mesh.Add(Element2D(fd_layer,elpids4))          
            else:
                elpids3 = [pids_middle[i],pids_middle[i+1],pids_refined_center[i] ]              
                elpids5 = [pids_middle[i+1],pids_end[i+1],pids_refined_center[i]]
                elpids7 = [pids_refined_center[i],pids_end[i+1],pids_refined_end[i]]

                elpids4 = [pids_middle[i],pids_refined_center[i],pids_end[i]]
                elpids6 = [pids_refined_center[i],pids_refined_end[i],pids_end[i]]
                
                mesh.Add(Element2D(fd_layer,elpids3)) 
                mesh.Add(Element2D(fd_layer,elpids4)) 
                mesh.Add(Element2D(fd_layer,elpids5)) 
                mesh.Add(Element2D(fd_layer,elpids6)) 
                mesh.Add(Element2D(fd_layer,elpids7)) 


        for i in range(nx):
            mesh.Add(Element1D([pids_start[i], pids_start[i+1]], index=bc))
            
        mesh.SetBCName(bc-1,"layer"+str(domin+1)+"-bnd")

        if refine_layer:
            pids_next_lvl = []
            for i in range(len(pids_end)):
                pids_next_lvl.append(pids_end[i])
                if i < len(pids_refined_end):
                    pids_next_lvl.append(pids_refined_end[i])
            return nx*2,pids_next_lvl
        else:
            return nx,pids_end


    pids_next_lvl = pids_inner

    for i in range(n_layers-1):

        #refine_layer = True if (i >= 4 and i <= 7) else False # Mesh-RR
        #refine_layer = True if (i >= 5 and i <= 8) else False # Mesh-R
        #refine_layer = False
        nx,pids_next_lvl = MakeLayer(quads=quads[i],nx=nx,mesh=mesh,pids_next_lvl=pids_next_lvl,r_start=r_layer[i+1],r_end=r_layer[i+2], 
                       mapping = mapping,
                       refine_layer=refine_layer[i],
                       domin = n_layers-2-i,
                       bc =  n_layers-i 
                      )
    for i in range(nx):
        mesh.Add(Element1D([pids_next_lvl[i], pids_next_lvl[i+1]], index=1))
    mesh.SetBCName(0,"outer")

    #if theta_bnd == 'periodic': 
    #    for j in range(len(slavej)):        
    #        mesh.AddPointIdentification(masterj[j],slavej[j],identnr=2,type=2) 
    #else:
    #    for j in range(len(slavej)-1):  
    #        mesh.Add(Element1D([slavej[j], slavej[j+1]], index=n_layers+2))
    #        mesh.Add(Element1D([masterj[j], masterj[j+1]], index=n_layers+3))
    #        
    #    mesh.SetBCName(n_layers+1,"left")
    #    mesh.SetBCName(n_layers+2,"right")


    mesh.Compress()       
    mesh = ngsolve.Mesh(mesh)
    
    return mesh,mesh_nodes
    
    from ngsolve import H1,GridFunction,CoefficientFunction,sqrt,x,y
    ## for higher order geometry 
    fes_P1 = H1(mesh, complex=False,  order=1,dirichlet=[])
    gfu = GridFunction (fes_P1)
    r_h = GridFunction (fes_P1)

    # map layers to P1
    r_h.vec[:] = 0.0

    for coord_nr,coord in mesh_nodes.items():
        r_h.vec[ coord_nr-1] = sqrt(coord[0]**2+coord[1]**2)

    #Draw (r_h, mesh, "rh")
    r_dist = sqrt(x**2+y**2)
    theta_perfect = CoefficientFunction(( (r_h - r_dist ) * x / r_dist, (r_h - r_dist ) * y / r_dist)) 
    #Draw (theta_perfect, mesh, "theta-perfect")

    fes_highOrder = H1(mesh, complex=False,  order=order_geom,dirichlet=[],dim=2)
    theta_h = GridFunction (fes_highOrder)
    theta_h.Set(theta_perfect)
    #Draw (theta_h, mesh, "theta-h")
    #input("Please press enter to apply mesh deformation")
    
    return mesh,theta_h



def Make1DMesh_refined(layer_nr,r_layer,nelem=100):
    
    mesh_1D = NetMesh(dim=1)
    pids = []   

    r_nodes = [ r_layer[-1-layer_nr] + j*(r_layer[-1] - r_layer[-1-layer_nr])/nelem for j in range(nelem +1)]
    
    #print("r_nodes =", r_nodes)

    for r_cord in r_nodes:
        pids.append (mesh_1D.Add (MeshPoint(Pnt(r_cord, 0, 0))))  

    n_mesh = len(pids)-1
    for i in range(n_mesh):
        mesh_1D.Add(Element1D([pids[i],pids[i+1]],index=1))
    mesh_1D.Add (Element0D( pids[0], index=1))
    mesh_1D.Add (Element0D( pids[n_mesh], index=2))
    mesh_1D.SetBCName(0,"left")
    mesh_1D.SetBCName(1,"right")
    mesh_1D = NGSMesh(mesh_1D)    
    return mesh_1D


def Make1DMesh_givenpts(mesh_r_pts):

    mesh_1D = NetMesh(dim=1)
    pids = []   
    for r_coord in mesh_r_pts:
        pids.append (mesh_1D.Add (MeshPoint(Pnt(r_coord, 0, 0))))                     
    n_mesh = len(pids)-1
    for i in range(n_mesh):
        mesh_1D.Add(Element1D([pids[i],pids[i+1]],index=1))
    mesh_1D.Add (Element0D( pids[0], index=1))
    mesh_1D.Add (Element0D( pids[n_mesh], index=2))
    mesh_1D.SetBCName(0,"left")
    mesh_1D.SetBCName(1,"right")
    mesh_1D = NGSMesh(mesh_1D)    
    return mesh_1D


from math import sqrt,pi,atan2
from numpy import arctan2
def inv_polar(vx,vy):
    r_radius = sqrt(vx**2+vy**2)
    phi_angle = arctan2(vy,vx)
    if phi_angle < 0:
        phi_angle += 2*pi
    #phi_angle = atan2(vx,vy)
    return r_radius,phi_angle

def HybridLayered_Mesh_U(r_layer,h_inner,quads,refine_layer,theta_bnd,x_max=pi,order_geom=5,automatic_refine=False):

    from math import sqrt
    #from ngsolve import NodeId,VERTEX

    #mapping = lambda x,y : ( y*sin(x),
    #                         y*cos(x))
   
    mapping = lambda x,y : ( y*cos(x),
                             y*sin(x))
    #mapping = None
    vertices_layer_nr = {}
    vertices_layer_coord = {}
    mesh_nodes = {}

    mesh = Mesh()
    mesh.dim=2

    n_layers = len(r_layer) - 1

    slavej = []
    masterj = []

    
    geo = SplineGeometry() 
    geo.AddCircle((0,0),r_layer[1],leftdomain=1,rightdomain=0,bc="inner")
    #mesh_inner = ngsolve.Mesh(geo.GenerateMesh(maxh=0.4))
    mesh_inner = geo.GenerateMesh(maxh=h_inner)

    # copy points from inner_mesh to mesh
    # pmap maps point numbers from inner_mesh to mesh
    pmap = { }
    pids_inner = []
    for e in mesh_inner.Elements2D():
        for vv in e.vertices:
            ##help(vv)
            if (vv not in pmap):
                #print(mesh_inner[vv].p) # gives coordinates of mesh point
                pmap[vv] = mesh.Add (mesh_inner[vv])
                mesh_nodes[pmap[vv].nr] = (mesh_inner[vv].p[0],mesh_inner[vv].p[1])

    # copy elements from inner_mesh to new mesh
    # map point-numbers:    
    idx_dom = mesh.AddRegion("layer"+str(n_layers-1), dim=2)
    #fd_layer = mesh.Add(FaceDescriptor(surfnr=1,domin=n_layers-1,bc=1))
    for e in mesh_inner.Elements2D():
        mesh.Add (Element2D (idx_dom, [pmap[vv] for vv in e.vertices]))
    
    for e in mesh_inner.Elements1D():
        for vv in e.vertices:
            point_nr = pmap[vv]
            if (point_nr not in pids_inner):
                pids_inner.append(point_nr)

    pids_inner.sort(key=lambda vv:inv_polar(mesh_nodes[vv.nr][0],mesh_nodes[vv.nr][1])[1] ) 
    pids_inner.append(pids_inner[0])

    def MakeLayer(quads, nx,mesh,pids_next_lvl,r_start,r_end,mapping = None,refine_layer=False,domin=1,bc=1):

        #fd_layer = mesh.Add(FaceDescriptor(surfnr=1,domin=domin,bc=1))
        idx_dom = mesh.AddRegion("layer"+str(domin), dim=2)

        pids_start = pids_next_lvl
        # compute angles of previous points
        pts_angles =[]
        zero_cnt = 0
        for nnode in pids_start:
            node_coord = mesh_nodes[nnode.nr]
            myangle = inv_polar(node_coord[0],node_coord[1])[1] 
            if abs(myangle) < 1e-13:
                if zero_cnt == 0:
                    pts_angles.append(myangle)
                    zero_cnt += 1
                else: 
                    pts_angles.append(2*pi)
            else:
                pts_angles.append(myangle)
        #print("pts_angles =" , pts_angles)

        pids_middle = []
        pids_end = []
        if refine_layer:
            pids_refined_center = []
            pids_refined_end = []
        r_middle = r_start + (r_end-r_start)/2
        for j in range(nx+1):
            x_middle,y_middle = pts_angles[j], r_middle
            x_end,y_end = pts_angles[j], r_end
            if mapping:
                x_middle,y_middle = mapping(x_middle,y_middle)
                x_end,y_end = mapping(x_end,y_end)  
            
            if j == nx:
                pids_middle.append(pids_middle[0])   
                pids_end.append(pids_end[0])   
            else:
                node_nr_middle = mesh.Add (MeshPoint(Pnt(x_middle,y_middle,0)))
                pids_middle.append(node_nr_middle)    
                node_nr_end = mesh.Add (MeshPoint(Pnt(x_end,y_end,0)))
                pids_end.append(node_nr_end)  
                mesh_nodes[node_nr_middle.nr] = (x_middle,y_middle)
                mesh_nodes[node_nr_end.nr] = (x_end,y_end)

        if quads and not refine_layer:
            for i in range(nx):
                elpids1 = [pids_start[i+1],pids_start[i],pids_middle[i],pids_middle[i+1] ]
                elpids2 = [pids_middle[i+1],pids_middle[i],pids_end[i],pids_end[i+1] ]
                mesh.Add(Element2D(idx_dom,elpids1)) 
                mesh.Add(Element2D(idx_dom,elpids2)) 
            for i in range(nx):
                mesh.Add(Element1D([pids_start[i], pids_start[i+1]], index=bc))
            mesh.SetBCName(bc-1,"layer"+str(domin+1)+"-bnd")
            return nx,pids_end

        if refine_layer:
            for j in range(nx):
                x_end_refined,y_end = 0.5*(pts_angles[j]+pts_angles[j+1]), r_end
                x_center_refined,y_center_refined = 0.5*(pts_angles[j]+pts_angles[j+1]), r_middle + 0.5*(r_end-r_middle)
                if mapping:
                    x_end_refined,y_end = mapping(x_end_refined,y_end)
                    x_center_refined,y_center_refined = mapping(x_center_refined,y_center_refined)
                
                node_nr_end_refined = mesh.Add (MeshPoint(Pnt(x_end_refined,y_end,0)))
                pids_refined_end.append(node_nr_end_refined) 

                node_nr_center_refined = mesh.Add (MeshPoint(Pnt(x_center_refined,y_center_refined,0)))
                pids_refined_center.append(node_nr_center_refined) 

                mesh_nodes[node_nr_end_refined.nr] = (x_end_refined,y_end)
                mesh_nodes[node_nr_center_refined.nr]  = (x_center_refined,y_center_refined)
                                    
        
        for i in range(nx):

            elpids1 = [pids_start[i+1],pids_start[i],pids_middle[i] ]
            elpids2 = [pids_start[i+1],pids_middle[i],pids_middle[i+1] ]

            mesh.Add(Element2D(idx_dom,elpids1)) 
            mesh.Add(Element2D(idx_dom,elpids2))   

            if not refine_layer:
                elpids3 = [pids_middle[i+1],pids_middle[i],pids_end[i] ]
                elpids4 = [pids_middle[i+1],pids_end[i],pids_end[i+1] ]
                mesh.Add(Element2D(idx_dom,elpids3)) 
                mesh.Add(Element2D(idx_dom,elpids4))          
            else:
                
                elpids3 = [pids_middle[i+1],pids_middle[i],pids_refined_center[i] ]              
                elpids5 = [pids_middle[i+1], pids_refined_center[i], pids_end[i+1] ]
                elpids7 = [pids_refined_center[i],pids_refined_end[i],pids_end[i+1] ]

                elpids4 = [pids_middle[i],pids_end[i],pids_refined_center[i]]
                elpids6 = [pids_refined_center[i],pids_end[i],pids_refined_end[i]]
                
                mesh.Add(Element2D(idx_dom,elpids3)) 
                mesh.Add(Element2D(idx_dom,elpids4)) 
                mesh.Add(Element2D(idx_dom,elpids5)) 
                mesh.Add(Element2D(idx_dom,elpids6)) 
                mesh.Add(Element2D(idx_dom,elpids7)) 


        for i in range(nx):
            mesh.Add(Element1D([pids_start[i], pids_start[i+1]], index=bc))
            
        mesh.SetBCName(bc-1,"layer"+str(domin+1)+"-bnd")

        if refine_layer:
            pids_next_lvl = []
            for i in range(len(pids_end)):
                pids_next_lvl.append(pids_end[i])
                if i < len(pids_refined_end):
                    pids_next_lvl.append(pids_refined_end[i])
            return nx*2,pids_next_lvl
        else:
            return nx,pids_end


    pids_next_lvl = pids_inner
    nx  = len(pids_inner)-1

    for i in range(n_layers-1):

        refinement_marker =  refine_layer[i]
        if automatic_refine:
            #if pi/nx > 2.0*0.5*abs( r_layer[i+2] - r_layer[i+1] ):
            if pi/nx > 1.5*0.5*abs( r_layer[i+2] - r_layer[i+1] ):
                refinement_marker = True
            else:
                refinement_marker = False
        nx,pids_next_lvl = MakeLayer(quads=quads[i],nx=nx,mesh=mesh,pids_next_lvl=pids_next_lvl,r_start=r_layer[i+1],r_end=r_layer[i+2], 
                       mapping = mapping,
                       refine_layer=refinement_marker,
                       domin = n_layers-2-i,
                       bc =  n_layers-i 
                      )
    for i in range(nx):
        mesh.Add(Element1D([pids_next_lvl[i], pids_next_lvl[i+1]], index=1))
    mesh.SetBCName(0,"outer")

    mesh.Compress()       
    mesh = ngsolve.Mesh(mesh)
    
    #return mesh,mesh_nodes
    
    from ngsolve import H1,GridFunction,CoefficientFunction,sqrt,x,y
    ## for higher order geometry 
    fes_P1 = H1(mesh, complex=False,  order=1,dirichlet=[])
    gfu = GridFunction (fes_P1)
    r_h = GridFunction (fes_P1)

    # map layers to P1
    r_h.vec[:] = 0.0

    for coord_nr,coord in mesh_nodes.items():
        r_h.vec[ coord_nr-1] = sqrt(coord[0]**2+coord[1]**2)

    #Draw (r_h, mesh, "rh")
    r_dist = sqrt(x**2+y**2)
    theta_perfect = CoefficientFunction(( (r_h - r_dist ) * x / r_dist, (r_h - r_dist ) * y / r_dist)) 
    #Draw (theta_perfect, mesh, "theta-perfect")

    fes_highOrder = H1(mesh, complex=False,  order=order_geom,dirichlet=[],dim=2)
    theta_h = GridFunction (fes_highOrder)
    theta_h.Set(theta_perfect)
    #Draw (theta_h, mesh, "theta-h")
    #input("Please press enter to apply mesh deformation")
    
    return mesh,theta_h


def angle2colat(angle):
    if angle >= 0 and angle <= pi/2: 
        return pi/2 - angle 
    elif angle >= 3*pi/2 and angle <= 2*pi:
        return (2*pi - angle) + pi/2 
    else:
        print("Invalid angle given as input = {0}".format(angle))
        return None 


def HybridLayered_Mesh_halfDisk(r_layer,h_inner,quads,refine_layer,theta_bnd,x_max=pi,order_geom=5,automatic_refine = False):

    from math import sqrt

    #mapping = lambda x,y : ( y*sin(x),
    #                         y*cos(x))
        
    mapping = lambda x,y : ( y*sin(x),
                             y*cos(x))
    #mapping = None
    vertices_layer_nr = {}
    vertices_layer_coord = {}
    mesh_nodes = {}

    mesh = Mesh()
    mesh.dim=2

    n_layers = len(r_layer) - 1

    slavej = []
    masterj = []

    axis_pnts = [] 
    
    geo = SplineGeometry() 
    #geo.AddCircle((0,0),r_layer[1],leftdomain=1,rightdomain=0,bc="inner")
    #mesh_inner = ngsolve.Mesh(geo.GenerateMesh(maxh=0.4))

    inner_index = 1
    outer_index = 0
    r_inner = r_layer[1]
    pnts = []
    pnts.extend([ (0,-r_inner), (r_inner,-r_inner), (r_inner,0), (r_inner,r_inner), (0,r_inner), (-r_inner,r_inner), (-r_inner,0), (-r_inner,-r_inner) ])
    pnums = [geo.AppendPoint(*p) for p in pnts]
    #pnums.append(spherical.AppendPoint(*(0,0)))
    first_segments = geo.Append( ["spline3", pnums[0],pnums[1],pnums[2]], leftdomain=inner_index, rightdomain=outer_index, maxh=h_inner, bc="inner-ring") 
    snd_segment = geo.Append( ["spline3", pnums[2],pnums[3],pnums[4]], leftdomain=inner_index, rightdomain=outer_index, maxh=h_inner, bc="inner-ring") 
    straight_bnd = geo.Append( ["line", pnums[4],pnums[0]], leftdomain=inner_index, rightdomain=outer_index, maxh=h_inner, bc="straight-bnd") 
    mesh_inner = geo.GenerateMesh(maxh=h_inner)
    #return  ngsolve.Mesh(mesh_inner)

    # copy points from inner_mesh to mesh
    # pmap maps point numbers from inner_mesh to mesh
    pmap = { }
    pids_inner = []
    for e in mesh_inner.Elements2D():
        for vv in e.vertices:
            ##help(vv)
            if (vv not in pmap):
                #print(mesh_inner[vv].p) # gives coordinates of mesh point
                pmap[vv] = mesh.Add (mesh_inner[vv])
                mesh_nodes[pmap[vv].nr] = (mesh_inner[vv].p[0],mesh_inner[vv].p[1])

    # copy elements from inner_mesh to new mesh
    # map point-numbers:    
    #fd_layer = mesh.Add(FaceDescriptor(surfnr=1,domin=n_layers-1,bc=1))
    #fd_layer = mesh.Add(FaceDescriptor(surfnr=1,domin=n_layers,bc=1))
    idx_dom = mesh.AddRegion("layer"+str(n_layers-1), dim=2)
    #mesh.SetMaterial(n_layers,"layer"+str(n_layers-1))
    
    for e in mesh_inner.Elements2D():
        mesh.Add (Element2D (idx_dom, [pmap[vv] for vv in e.vertices]))
    
    for e in mesh_inner.Elements1D():
        for vv in e.vertices:
            point_nr = pmap[vv]
            point_coord = mesh_inner[vv].p
            if (point_nr not in pids_inner and  abs( sqrt(point_coord[0]**2+point_coord[1]**2) -r_inner) < 1e-13 ):
                pids_inner.append(point_nr)
            if (point_nr not in axis_pnts) and ( abs(point_coord[0]) < 1e-14):
                axis_pnts.append(point_nr)

    pids_inner.sort(key=lambda vv: angle2colat(inv_polar(mesh_nodes[vv.nr][0],mesh_nodes[vv.nr][1])[1]) ) 
    #pids_inner.append(pids_inner[0])
    #mesh.SetMaterial(n_layers-1,"layer"+str(n_layers-1))

    def MakeLayer(quads, nx,mesh,pids_next_lvl,r_start,r_end,mapping = None,refine_layer=False,domin=1,bc=1):

        #print("domin = ", domin)
        #fd_layer = mesh.Add(FaceDescriptor(surfnr=1,domin=domin,bc=1))
        #fd_layer = mesh.Add(FaceDescriptor(surfnr=1,domin=domin+1,bc=1))
        #mesh.SetMaterial(domin+1,"layer"+str(domin))
        idx_dom = mesh.AddRegion("layer"+str(domin), dim=2)


        pids_start = pids_next_lvl
        # compute angles of previous points
        pts_angles =[]
        zero_cnt = 0
        for nnode in pids_start:
            node_coord = mesh_nodes[nnode.nr]
            myangle = angle2colat(inv_polar(node_coord[0],node_coord[1])[1])
            #if abs(myangle) < 1e-13:
            #    if zero_cnt == 0:
            #        pts_angles.append(myangle)
            #        zero_cnt += 1
            #    else: 
            #        pts_angles.append(2*pi)
            #else:
            pts_angles.append(myangle)
        #print("pts_angles =" , pts_angles)
        #input("")

        pids_middle = []
        pids_end = []
        if refine_layer:
            pids_refined_center = []
            pids_refined_end = []
        r_middle = r_start + (r_end-r_start)/2
        for j in range(nx+1):
            x_middle,y_middle = pts_angles[j], r_middle
            x_end,y_end = pts_angles[j], r_end
            if mapping:
                x_middle,y_middle = mapping(x_middle,y_middle)
                x_end,y_end = mapping(x_end,y_end)  
            
            #if j == nx:
            if False:
                pids_middle.append(pids_middle[0])   
                pids_end.append(pids_end[0])   
            else:
                node_nr_middle = mesh.Add (MeshPoint(Pnt(x_middle,y_middle,0)))
                pids_middle.append(node_nr_middle)    
                node_nr_end = mesh.Add (MeshPoint(Pnt(x_end,y_end,0)))
                pids_end.append(node_nr_end)  
                mesh_nodes[node_nr_middle.nr] = (x_middle,y_middle)
                mesh_nodes[node_nr_end.nr] = (x_end,y_end)
                if abs(pts_angles[j]) < 1e-14 or abs(pts_angles[j]-pi) < 1e-14:
                    axis_pnts.append(node_nr_middle)
                    axis_pnts.append(node_nr_end)

        if quads and not refine_layer:
            for i in range(nx):
                elpids1 = [pids_start[i],pids_start[i+1],pids_middle[i+1],pids_middle[i] ]
                elpids2 = [pids_middle[i],pids_middle[i+1],pids_end[i+1],pids_end[i] ]
                mesh.Add(Element2D(idx_dom,elpids1)) 
                mesh.Add(Element2D(idx_dom,elpids2)) 
            for i in range(nx):
                mesh.Add(Element1D([pids_start[i], pids_start[i+1]], index=bc))
            mesh.SetBCName(bc-1,"layer"+str(domin+1)+"-bnd")
            #mesh.SetMaterial(domin,"layer"+str(domin))
            return nx,pids_end

        if refine_layer:
            for j in range(nx):
                x_end_refined,y_end = 0.5*(pts_angles[j]+pts_angles[j+1]), r_end
                x_center_refined,y_center_refined = 0.5*(pts_angles[j]+pts_angles[j+1]), r_middle + 0.5*(r_end-r_middle)
                if mapping:
                    x_end_refined,y_end = mapping(x_end_refined,y_end)
                    x_center_refined,y_center_refined = mapping(x_center_refined,y_center_refined)
                
                node_nr_end_refined = mesh.Add (MeshPoint(Pnt(x_end_refined,y_end,0)))
                pids_refined_end.append(node_nr_end_refined) 

                node_nr_center_refined = mesh.Add (MeshPoint(Pnt(x_center_refined,y_center_refined,0)))
                pids_refined_center.append(node_nr_center_refined) 

                mesh_nodes[node_nr_end_refined.nr] = (x_end_refined,y_end)
                mesh_nodes[node_nr_center_refined.nr]  = (x_center_refined,y_center_refined)
                                    
        
        for i in range(nx):
            
            elpids1 = [pids_start[i],pids_start[i+1],pids_middle[i] ]
            elpids2 = [pids_start[i+1],pids_middle[i+1],pids_middle[i] ]

            mesh.Add(Element2D(idx_dom,elpids1)) 
            mesh.Add(Element2D(idx_dom,elpids2))   

            if not refine_layer:
                elpids3 = [pids_middle[i],pids_middle[i+1],pids_end[i] ]
                elpids4 = [pids_middle[i+1],pids_end[i+1],pids_end[i] ]
                mesh.Add(Element2D(idx_dom,elpids3)) 
                mesh.Add(Element2D(idx_dom,elpids4))          
            else:
                elpids3 = [pids_middle[i],pids_middle[i+1],pids_refined_center[i] ]              
                elpids5 = [pids_middle[i+1],pids_end[i+1],pids_refined_center[i]]
                elpids7 = [pids_refined_center[i],pids_end[i+1],pids_refined_end[i]]

                elpids4 = [pids_middle[i],pids_refined_center[i],pids_end[i]]
                elpids6 = [pids_refined_center[i],pids_refined_end[i],pids_end[i]]
                 
                mesh.Add(Element2D(idx_dom,elpids3)) 
                mesh.Add(Element2D(idx_dom,elpids4)) 
                mesh.Add(Element2D(idx_dom,elpids5)) 
                mesh.Add(Element2D(idx_dom,elpids6)) 
                mesh.Add(Element2D(idx_dom,elpids7)) 


        for i in range(nx):
            mesh.Add(Element1D([pids_start[i], pids_start[i+1]], index=bc))
            
        mesh.SetBCName(bc-1,"layer"+str(domin+1)+"-bnd")
        #mesh.SetMaterial(domin,"layer"+str(domin)) 

        if refine_layer:
            pids_next_lvl = []
            for i in range(len(pids_end)):
                pids_next_lvl.append(pids_end[i])
                if i < len(pids_refined_end):
                    pids_next_lvl.append(pids_refined_end[i])
            return nx*2,pids_next_lvl
        else:
            return nx,pids_end


    pids_next_lvl = pids_inner
    nx  = len(pids_inner)-1

    for i in range(n_layers-1):

        refinement_marker =  refine_layer[i]
        if automatic_refine:
            #if pi/nx > 2.0*0.5*abs( r_layer[i+2] - r_layer[i+1] ):
            if pi/nx > 1.5*0.5*abs( r_layer[i+2] - r_layer[i+1] ):
            #if pi/nx > 1.5*0.5*abs( r_layer[i+2] - r_layer[i+1] ):
                #print("pi/nx = {0},  0.5*( r_layer[i+2] - r_layer[i+1] )  ={1}".format(pi/nx,0.5*abs( r_layer[i+2] - r_layer[i+1] )))
                #print("Setting refinement marker")
                refinement_marker = True
            else:
                refinement_marker = False

        nx,pids_next_lvl = MakeLayer(quads=quads[i],nx=nx,mesh=mesh,pids_next_lvl=pids_next_lvl,r_start=r_layer[i+1],r_end=r_layer[i+2], 
                       mapping = mapping,
                       refine_layer=refinement_marker,
                       domin = n_layers-2-i,
                       bc =  n_layers-i 
                      )
    for i in range(nx):
        mesh.Add(Element1D([pids_next_lvl[i], pids_next_lvl[i+1]], index=1))
    mesh.SetBCName(0,"outer")

    axis_pnts.sort(key=lambda vv: mesh_nodes[vv.nr][1],reverse=True) 
    for i in range(len(axis_pnts)-1):
        mesh.Add(Element1D([axis_pnts[i], axis_pnts[i+1]], index=n_layers+1))
    mesh.SetBCName(n_layers,"axis_bnd")

    mesh.Compress()       
    mesh = ngsolve.Mesh(mesh)
    
    #return mesh,mesh_nodes
    
    from ngsolve import H1,GridFunction,CoefficientFunction,sqrt,x,y
    ## for higher order geometry 
    fes_P1 = H1(mesh, complex=False,  order=1,dirichlet=[])
    gfu = GridFunction (fes_P1)
    r_h = GridFunction (fes_P1)

    # map layers to P1
    r_h.vec[:] = 0.0

    for coord_nr,coord in mesh_nodes.items():
        r_h.vec[ coord_nr-1] = sqrt(coord[0]**2+coord[1]**2)

    #Draw (r_h, mesh, "rh")
    r_dist = sqrt(x**2+y**2)
    theta_perfect = CoefficientFunction(( (r_h - r_dist ) * x / r_dist, (r_h - r_dist ) * y / r_dist)) 
    #Draw (theta_perfect, mesh, "theta-perfect")

    fes_highOrder = H1(mesh, complex=False,  order=order_geom,dirichlet=[],dim=2)
    theta_h = GridFunction (fes_highOrder)
    theta_h.Set(theta_perfect)
    #Draw (theta_h, mesh, "theta-h")
    #input("Please press enter to apply mesh deformation")
    
    return mesh,theta_h

