from math import pi
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from scipy.integrate import simps 
from scipy.special import eval_legendre

generate_plots_for_check = False 

plt.rc('legend', fontsize=18)
plt.rc('axes', titlesize=18)
plt.rc('axes', labelsize=18)
plt.rc('xtick', labelsize=14)    
plt.rc('ytick', labelsize=14)

power_MJ = np.loadtxt('../powerl_MJ.txt')
ell_MJ = np.arange(len(power_MJ))
ell_MJe = np.arange(len(power_MJ)+1)
coef_MJ = np.polyfit(ell_MJ,power_MJ,4)
filter_ell = np.polyval(coef_MJ,ell_MJe)
#plt.figure()
#plt.plot(ell_MJ, power_MJ)
#plt.plot(ell_MJe,filter_ell)
#plt.show()

def PrecomputeFactors( power_spectrum,inp_freq, f_hz):

    Nt = 2**13
    N_theta = 1000
    thetas = np.linspace(0,pi,N_theta)

    ts = np.linspace(0,300,Nt)
    tss =  60*ts/(2*pi)
    ts_minutes = ts 
    ts_seconds = 60*ts 
    min2seconds = 60
    omega_center = inp_freq*1e-3*2*pi
    
    def F_ell(l):
        return filter_ell[l]

    def PI_scal(f):
        result = 1 + ((abs(f)-3.3*1e-3*2*pi)/ (0.6*1e-3*2*pi) )**2
        return 1/result 
    
    def Gauss_scale(f):        
        return np.exp( -0.5*(abs(f)-omega_center)**2/(0.6*1e-3*2*pi)**2 ) # standard choice 

    precomputed_factors = np.zeros( power_spectrum.shape) 

    for i in range(power_spectrum.shape[0]):
        for l in range(power_spectrum.shape[1]):
            if abs(f_hz[i]) > 1e-13:                    
                precomputed_factors[i,l] = (1/(f_hz[i]))*PI_scal(f_hz[i])*F_ell(l)*Gauss_scale(f_hz[i])**2 #this one 
            else:
                precomputed_factors[i,l] = PI_scal(f_hz[i])*F_ell(l)*Gauss_scale(f_hz[i])**2 #this one
    return precomputed_factors


def compute_TD_diagram(power_spectrum,inp_freq,f_hz,name_str="dummy.png",only_omega=True,W_tau=False,obs=False,idxx=[pi/6],idxx_label=["30deg"],
                      N_start_fac=0, N_stop_fac=1, only_comp_required_theta = True, precomputed_factors = [] ):

    Nt = 2**13
    N_theta = 1000
    thetas = np.linspace(0,pi,N_theta)
    if only_comp_required_theta:
        thetas_idx_range = idxx
    else:
        thetas_idx_range = range(len(thetas))


    ts = np.linspace(0,300,Nt)
    tss =  60*ts/(2*pi)
    ts_minutes = ts 
    ts_seconds = 60*ts 
    min2seconds = 60
    omega_center = inp_freq*1e-3*2*pi
    domega = f_hz[-1]/power_spectrum.shape[0]
    dt = tss[-1]/(Nt)
    dt_phys = 2*pi*dt
    Ntmp = int(1/(domega*dt))
    max_vals = [1.0 for i in range(len(idxx ))]

    def F_ell(l):
        return filter_ell[l]
    #def F_ell(l):
    #    if l > 300:
    #        return 0.0
    #    else:
    #        return filter_ell[l]
    
    def PI_scal(f):
        result = 1 + ((abs(f)-3.3*1e-3*2*pi)/ (0.6*1e-3*2*pi) )**2
        return 1/result 
    
    def Gauss_scale(f):        
        return np.exp( -0.5*(abs(f)-omega_center)**2/(0.6*1e-3*2*pi)**2 ) # standard choice 

    C_tau = np.zeros(( power_spectrum.shape[0], len(thetas)), dtype="float")    
    C_tau_unscaled = np.zeros(( power_spectrum.shape[0], len(thetas)), dtype="float")
    
    #if W_tau or obs:
    if len(precomputed_factors) == 0: 
        precomputed_factors = np.zeros( power_spectrum.shape) 
        #precomputed_factors_unscaled = np.zeros( power_spectrum.shape) 
        
        for i in range(power_spectrum.shape[0]):
            for l in range(power_spectrum.shape[1]):
                if abs(f_hz[i]) > 1e-13:                    
                    precomputed_factors[i,l] = (1/(f_hz[i]))*PI_scal(f_hz[i])*F_ell(l)*Gauss_scale(f_hz[i])**2 #this one 
                else:
                    precomputed_factors[i,l] = PI_scal(f_hz[i])*F_ell(l)*Gauss_scale(f_hz[i])**2 #this one
    
    for j in thetas_idx_range:
        l_legendre = np.array([(l+0.5)*eval_legendre(l,np.cos(thetas[j])) for l in range(power_spectrum.shape[1]) ])
        #print("thetas[j] = ", thetas[j])
        for i in range(power_spectrum.shape[0]):
            li_factor = np.multiply(precomputed_factors[i,:],l_legendre)
            C_tau[i,j] = np.dot(power_spectrum[i,:],li_factor)

    C_t = np.zeros((Nt,len(thetas)), dtype="complex")
    C_t_normalized = np.zeros((Nt,len(thetas)), dtype="complex")

    if not W_tau:
        for j in thetas_idx_range:  
            C_t[:,j] = np.fft.fft(C_tau[:,j],n=Ntmp)[:Nt]
            C_t[:,j] = C_t[:,j].real 

    if W_tau: 
        
        Nstart = int(N_start_fac[inp_freq]*Nt)
        Nstop = int(N_stop_fac[inp_freq]*Nt)
        normalizations  = np.zeros(len(thetas), dtype="float")
        for j in thetas_idx_range:  
            C_t[:,j] = np.fft.fft(C_tau[:,j],n=Ntmp)[:Nt]
            
            normalized_C = C_t[:,j].real/ np.max(np.abs(C_t[Nstart:Nstop,j].real))

            normalizations[j] =   np.max(np.abs(C_t[Nstart:Nstop,j].real))
            C_t_normalized[:,j] = normalized_C 
            C_t[:,j] = normalized_C 
         
        C_partial_t = np.zeros((Nt,len(thetas)), dtype="float") 
        weights_t  = np.zeros(( Nt, len(thetas)), dtype="float")
        

        W_tau = np.zeros(( Nt, len(thetas)), dtype="float")

        # computing time derivative of cross-covariance
        k2 = np.fft.fftfreq(Nt) * 2*np.pi * Nt / (ts[-1] - ts[0])
        for j in thetas_idx_range:
             #C_partial_t[:,j] =  np.fft.ifft(1j*k2*np.fft.fft(C_t[:,j]  ) )
             C_partial_t[:,j] =  np.gradient(C_t[:,j], ts[1]-ts[0]) 

        window_header = "t  " 
        cross_header = "t  " 
        plot_collect_window = [ts]
        plot_collect_cross  = [ts]
        for j in thetas_idx_range:
            if j in idxx:
                cross_header += idxx_label[idxx.index(j)]
                window_header += idxx_label[idxx.index(j)]
                maxi = max_vals[idxx.index(j)]
                normalized_C = C_t_normalized[:,j] 
                
                if generate_plots_for_check:         
                    plt.plot(ts, C_t[:,j],label="C" )
                    plt.plot(ts, C_partial_t[:,j] , label="Ct-FFT" )
                    plt.title("C and time deriv")
                    plt.legend()
                    plt.show()

                
                tg = ts[  np.where( np.abs(normalized_C[Nstart:Nstop]) >= maxi )[0][0] + Nstart ]
                sigma = 12

                weights = np.piecewise(ts, [ np.abs(ts - tg) < 10,  np.abs(ts - tg) > 10,], [1, 0])

                if generate_plots_for_check:         
                    plot_collect_window.append(weights)
                    plot_collect_cross.append(normalized_C)
                    plt.plot(ts,weights,label="weights")
                    plt.plot(ts,normalized_C,label="C")
                    plt.legend()
                    plt.show()
            else:
                weights = np.ones(len(ts))

            weights_t[:,j] = weights[:]
            normalization_j = simps(weights[:]*C_partial_t[:,j]**2,x=ts_minutes)
            W_tau[:,j] = -weights[:]*C_partial_t[:,j]/normalization_j 
    
        return C_t, C_partial_t,  W_tau, weights_t, normalizations  
 
    if obs: 
        C_t = np.zeros((Nt,len(thetas)), dtype="complex")
         
        window_header = "t  " 
        cross_header = "t  " 
        plot_collect_window = [ts]
        plot_collect_cross  = [ts]
        for j in thetas_idx_range:  
            if j in idxx:
                cross_header += idxx_label[idxx.index(j)]
                window_header += idxx_label[idxx.index(j)] 
                C_t[:,j] = np.fft.fft(C_tau[:,j],n=Ntmp)[:Nt]
                normalized_C = C_t[:,j].real/ np.max(np.abs(C_t[:,j].real))
                C_t[:,j] = normalized_C
                plot_collect_cross.append(normalized_C)

                if generate_plots_for_check:         
                    plt.plot(ts,normalized_C,label="C")
                    plt.legend()
                    plt.show()
        
        np.savetxt(fname = "cross-obs.dat", 
                       X = np.transpose(plot_collect_cross),
                       header = cross_header,
                       comments = '')
            
        return C_t
 
    if only_omega:
        return C_t


def normalize_C_comp(C_t_comp,scale_facs,thetas):
    for j in range(len(thetas)):  
        C_t_comp[:,j] *= 1/ scale_facs[j]
