from netgen.csg import *
from ngsolve import *
import numpy as np
import sys
sys.path.append("/home/janosch/projects/helio-lie/examples")
from sweeping_geom import HybridLayered_Mesh_halfDisk,Make1DMesh_givenpts
from math import pi

end_ModelS = 1.000731467456897006
end_VALC = 1.00365 


if ( len(sys.argv) > 1 and int(sys.argv[1]) in  [2,3]  ):
    space_dim = int(sys.argv[1])
    print("dim = ", space_dim)
else:
    raise ValueError('Invalid dimension!')

def count_freedofs(dof_ar):
    cnt = 0
    for dof in dof_ar:
        if dof: 
            cnt +=1 
    return cnt 

#space_dim = 2 

a_bnd = 1.0

if space_dim == 2:
    fmhz = 0.00341 # at this frequency there is one wavelen in VAL-C atmosphere
else:
    fmhz = 0.00341 # at this frequency there is 0.8 wavelen in VAL-C atmosphere
#fmhz = 0.008

#fmhz = 0.00165
RSun =  6.96*10**8
f_scaled = fmhz*2*pi*RSun

if space_dim == 3:
    order = 10
else:
    order = 5


a = 1.0 
def getV_VALC_model(spline_order=5):
    rS,cS,rhoS,_,__ = np.loadtxt("modelS_plusVAL_smooth.txt",unpack=True)
    ind = np.argsort(rS)
    r = rS[ind]
    c = cS[ind]
    rho = rhoS[ind]
    weight = np.ones(len(r))
    r = r.tolist()
    c = c.tolist()
    rho = rho.tolist()
    rho_clean = rho
    c_clean = c

    r_beg = r[0]
    rho_beg = rho_clean[0]
    c_beg = c_clean[0]

    r_orig = r
    c_orig = c

    r = [r_beg for i in range(spline_order+1)]  + r
    c_clean =  [c_beg for i in range(spline_order+1)] + c_clean
    rho_clean =  [rho_beg for i in range(spline_order+1)] + rho_clean
    c_cleanBB = BSpline(spline_order,r,c_clean)
    rho_cleanBB = BSpline(spline_order,r,rho_clean)

    return c_cleanBB, rho_cleanBB



def calc_ndof_prop(ra,f_hz):

    cBB_ext,rhoBB_ext = getV_VALC_model()
    c_1D = cBB_ext(x)
    lambda_eff  = 2*pi*c_1D/f_hz
    w_mesh_pts = np.linspace(0.0,ra,1000).tolist()
    mesh_1D_w = Make1DMesh_givenpts(w_mesh_pts)
    ndof_prop = Integrate(1/lambda_eff**space_dim,mesh_1D_w,order=10)
    if space_dim == 2:
        return 2*pi*ndof_prop
    if space_dim == 3:
        return 4*pi**2*ndof_prop 


def get_rlayer(f_hz_mesh):
    
    cBB_ext,rhoBB_ext = getV_VALC_model()
    #Determine position of layers according to local wavelength
    c_1D = cBB_ext(x)
    lambda_eff  = 2*pi*c_1D/f_hz_mesh
    w_mesh_pts = np.linspace(0.0,a_bnd,1000).tolist()
    mesh_1D_w = Make1DMesh_givenpts(w_mesh_pts)
    N_wavelen = Integrate(1/lambda_eff,mesh_1D_w,order=10)
    print("N_wavelen = ", N_wavelen) 
    r_samples = np.linspace(0.0,0.8,30,endpoint=False).tolist() + np.linspace(0.8,0.95,50,endpoint=False).tolist() + np.linspace(0.95,0.98,100,endpoint=False).tolist() + np.linspace(0.98,a_bnd,220).tolist()
    N_wavelen_r = []
    for r_sample in r_samples:
        N_wavelen_r.append(Integrate(IfPos(r_sample-x,1.0,0.0)*1/lambda_eff,mesh_1D_w))
    r_layer = [0.0]
    N_current = 1
    N_layer = []
    for r_sample,N_sample in zip(r_samples,N_wavelen_r):
        if N_sample > N_current:
            r_layer.append(r_sample)
            N_layer.append(N_sample)
            N_current += 1
    if abs(N_layer[-1]-N_wavelen) < 0.25:
        print("Replace last wavelen mesh point with 1.0")
        r_layer[-1] = a_bnd
    else:
        r_layer.append(a_bnd)
    rs_to_surface = r_layer.copy() 
    print("rs_to_surface = ", rs_to_surface )

    return rs_to_surface

rs_to_surface  = get_rlayer( f_scaled ) 


def get_3D_mesh(rs):
    nlayers = len(rs)
    layer_idx = nlayers-1
    geo = CSGeometry()
    ri = rs[0] 
    sphere_i = Sphere(Pnt(0,0,0),rs[0])
    sphere_i.mat("layer{0}".format(layer_idx))
    sphere_i.bc("layer{0}-bnd".format(layer_idx))
    geo.Add(sphere_i,maxh=rs[0]/3)
    layer_idx -= 1
    
    for j in range(1,len(rs)):
        ro = rs[j]
        h_layer = (ro-ri) 
        sphere_i = Sphere(Pnt(0,0,0),ri)
        sphere_o = Sphere(Pnt(0,0,0),ro)
        #if j == (len(rs)-2):
        #    sphere_o.bc("layer1-bnd")
        if j == (len(rs)-1):
            sphere_o.bc("outer")
            sphere_i.bc("layer1-bnd-double")
        else:
            sphere_o.bc("layer{0}-bnd".format(layer_idx) )
            sphere_i.bc("layer{0}-bnd-double".format(layer_idx+1) )

        outer_ring = sphere_o - sphere_i
        outer_ring.mat("layer{0}".format(layer_idx))
        geo.Add(outer_ring,maxh= (ro-ri) )
        ri = ro
        layer_idx -= 1

    ngmesh = geo.GenerateMesh(maxh=2.0)
    return Mesh(ngmesh)

def get_2D_mesh(rs):
    r_layer = rs 
    #r_start = 0
    nx = 2*len(r_layer)
    refinement_marker = [False for i in range(len(r_layer))]
    Mc = 1
    for i in range(Mc+1):
        if i < len(refinement_marker):
            refinement_marker[i] = True
    quads_marker = [False for i in range(len(r_layer))]
    order_geom = order
    h_inner = 0.5*(r_layer[1]-r_layer[0])
    order_geom = order
    #print("r_layer =" , r_layer)
    mesh,theta_h = HybridLayered_Mesh_halfDisk(r_layer,h_inner,quads_marker,refinement_marker,'D',2*pi,order_geom,True)
    return mesh 



if space_dim == 3: 
    mesh = get_3D_mesh(rs_to_surface[1:] )
elif space_dim == 2: 
    mesh = get_2D_mesh(rs_to_surface[1:] )


if space_dim == 2:

    nlayer = len( mesh.GetMaterials())
    mat_inner = mesh.Materials("layer0")
    for i in range(1,nlayer):
        print("layer{0}".format(i))
        mat_inner += mesh.Materials("layer{0}".format(i))

    # Note: axis_bnd is counted as bnd for layer0 but not for layeri with i > 1. 
    #       Therefore, we omit these dofs here while counting

    print("------------------------------------------------------------")
    print("meshed_atmo.GetMaterials() = ", mesh.GetMaterials())
    print("meshed_atmo.GetBoundaries() = ", mesh.GetBoundaries())
    
    #fes_full = Compress(H1(meshed_atmo,complex=True,order=order,dirichlet='axis_bnd'))
    #ndof_full = count_freedofs(fes_full.FreeDofs())  

    fes_inner = Compress(H1(mesh,complex=True,order=order,definedon=mat_inner,dirichlet='axis_bnd'))
    fes_surf = Compress(H1(mesh,order=order,complex=True,definedon=mesh.Boundaries("outer")))
    
    
    ndof_inner =  count_freedofs(fes_inner.FreeDofs())  
    ndof_surf = count_freedofs(fes_surf.FreeDofs())  
    

     
    #print("fes_meshed_atmo_ndof - fes_inner_ndof = {0},   = {1}".format( fes_meshed_atmo_ndof - fes_inner_ndof,  fes_meshed_sub.ndof  ))

if space_dim == 3: 
    print("------------------------------------------------------------")
    #print("meshed_atmo.GetMaterials() = ", meshed_atmo.GetMaterials())
    #print("meshed_atmo.GetBoundaries() = ", meshed_atmo.GetBoundaries())

    nlayer = len( mesh.GetMaterials())
    mat_inner = mesh.Materials("layer0")
    for i in range(1,nlayer):
        #print("layer{0}".format(i))
        mat_inner += mesh.Materials("layer{0}".format(i))

    fes_inner = Compress(H1(mesh,complex=True,order=order,definedon=mat_inner,dirichlet=[]))
    ndof_inner =  count_freedofs(fes_inner.FreeDofs())  

    fes_surf = Compress(H1(mesh,order=order,complex=True,definedon=mesh.Boundaries("outer")))
    ndof_surf = count_freedofs(fes_surf.FreeDofs())  



ndof_analytic_inner =  calc_ndof_prop(ra=1.0,f_hz= f_scaled ) 
C_prop = ndof_inner/ndof_analytic_inner 
  
ndof_VALC = C_prop * calc_ndof_prop(ra= end_VALC ,f_hz= f_scaled ) 
ndof_ModelS = C_prop * calc_ndof_prop(ra= end_ModelS, f_hz= f_scaled )

print("ndof_inner = ", ndof_inner)
print("ndof_surf = ", ndof_surf)
print("ndof_VALC  = ", ndof_VALC  )
print("ndof_ModelS = ", ndof_ModelS)  


for N in range(4):
    print("N = {0}, increase = {1} %".format(N, round( 100*N*ndof_surf/ndof_inner,1)  )) 

print("Increase from meshing VAL-C atmosphere = {0} %".format(  round(100*(ndof_VALC - ndof_inner  )/ndof_inner,1)  ) )
print("Increase from meshing Model S atmosphere = {0} %".format( round(100* (ndof_ModelS - ndof_inner  )/ndof_inner,1 )  ) )

