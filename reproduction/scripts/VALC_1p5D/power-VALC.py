from netgen.meshing import *
from netgen.csg import *
from netgen.meshing import Mesh as netmesh
from math import pi,ceil
import numpy as np
import matplotlib.pyplot as plt
from scipy.sparse import csr_matrix
from ngsolve import Mesh as NGSMesh

import sys,os
file_dir = os.path.dirname(os.path.realpath(__file__))
upfolder_path=  os.path.abspath(os.path.join( os.path.dirname( __file__ ), '..' ))
sys.path.append(upfolder_path)
from time_distance_helio import filter_ell

from cmath import sqrt as csqrt
np.random.seed(seed=1)
from ngsolve import *
ngsglobals.msg_level = 0
SetNumThreads(4)

#################################################### 
# parameters
####################################################


atmospheric_models = ["VALC","Atmo","extension"]
#atmospheric_models = ["VALC","Atmo"]
atmospheric_model = atmospheric_models[0]
dtn_approximations = ["exact","learned","learned-exp"]
N_compute = 0
idx_a = 0

if ( len(sys.argv) > 1 and sys.argv[1] in dtn_approximations + ["only_learning"]  ):
    dtn_approx = sys.argv[1]
    #idx_a = int(sys.argv[2])
else:
    raise ValueError('I do not know this atmospheric Model!')
if ( dtn_approx ==  "learned"):
    if (len(sys.argv) > 2 and int(sys.argv[2]) in range(10) ):
        N_compute = int(sys.argv[2])
if ( dtn_approx ==  "learned-exp"):
    if (len(sys.argv) > 2 and int(sys.argv[2]) in range(10) ):
        N_compute = int(sys.argv[2])
#if ( dtn_approx ==  "learned"):
#    if (len(sys.argv) > 2 and int(sys.argv[2]) in range(10) ):
#        N_compute = int(sys.argv[2])
#        if (len(sys.argv) > 3 and int(sys.argv[3]) in range(6)):
#                idx_a = int(sys.argv[3])
##        else:
#            raise ValueError("Invalid input for learned IE computation.") 
##    else:
#        raise ValueError("Invalid input for learned IE computation.") 

#dtn_approx = dtn_approximations[6]

order_ODE = 5 # order of ODE discretization
L_min = 0 
#L_max = 1000  # how many DtN numbers to take

L_max = 1000
#L_max = 20

#L_max = 500
L_range = range(L_min,L_max+1)

L_range = np.arange(L_min,L_max+1)

spline_order = order_ODE # order of Spline approx. for coefficients

#R_max_ODE = 1.0036 # to how far out the ODE for VAL-C model should be solved

# depracated 
R_start_PML = 1.001
R_min_ODE = 0.0

prec = 3000

use_PML = False

RSun =  6.96*10**8
a_Atmo = 1.000731467456897006
a_VALC = 1.00365
#a_list = [1.0, 1.0 + 0.5*(a_Atmo-1.0),a_Atmo]
#N_outer = 5
N_outer = 5

# submesh going to end of Atmo model
a_list = [  1.0 + (i/N_outer)*(a_Atmo-1.0) for i in range(N_outer+1)  ]
# extend mesh to end of VALC model
N_outer += 10
for i in range(1,N_outer+1):
    a_list.append( a_Atmo + (i/N_outer)*(a_VALC-a_Atmo)  )
print("a_list =", a_list)
a_str_list = ["{0}km".format( int(ceil( (aval-1.0)*RSun/1000)) ) for aval in a_list]
print("a_str_list = ", a_str_list)

a = 1.0
print("a = {0}".format(a))

Nmax = 3 # maximal dofs for learned IE in radial direction
#Nmax = 1 # maximal dofs for learned IE in radial direction
alpha_decay = 1/60 # exponential decay factor of weights
Nrs = list(range(Nmax))
show_plots = True
#solver = "pardiso"
solver = "sparsecholesky"
ansatz = "mediumSym"
flags = {"max_num_iterations":10000,
         "check_gradients":False, 
         "use_nonmonotonic_steps":True,
         "minimizer_progress_to_stdout":False,
         "num_threads":12,
         "report_level":"Brief",
         "function_tolerance":1e-12,
         "parameter_tolerance":1e-12,
         "gradient_tolerance":1e-12}

nodamping  = False

f_hzs = np.linspace(0,8.3,7200) # to compare with Atmo computations 
f_hzs = (10**-3)*f_hzs

#################################################### 
# some auxiliary functions
####################################################

def sqrt_2branch(z):
    if z.real < 0 and z.imag <0:
        return - csqrt(z)
    else:
        return csqrt(z)

def P_DoFs(M,test,basis):
    return (M[test,:][:,basis])


def Make1DMesh_givenpts(mesh_r_pts):

    mesh_1D = netmesh(dim=1)
    pids = []   
    for r_coord in mesh_r_pts:
        pids.append (mesh_1D.Add (MeshPoint(Pnt(r_coord, 0, 0))))                     
    n_mesh = len(pids)-1
    for i in range(n_mesh):
        mesh_1D.Add(Element1D([pids[i],pids[i+1]],index=1))
    mesh_1D.Add (Element0D( pids[0], index=1))
    mesh_1D.Add (Element0D( pids[n_mesh], index=2))
    mesh_1D.SetBCName(0,"left")
    mesh_1D.SetBCName(1,"right")
    mesh_1D = NGSMesh(mesh_1D)    
    return mesh_1D

def new_initial_guess(l1_old,l2_old,ansatz):
    N = l1_old.shape[0]
    l1_guess = np.zeros((N+1,N+1),dtype='complex')
    l2_guess = np.zeros((N+1,N+1),dtype='complex')
    if ansatz in ["medium","full"]:
        l1_guess = 1e-3*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))
        l2_guess = 1e-3*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))
        l1_guess[:N,:N] = l1_old[:]
        l2_guess[:N,:N] = l2_old[:]
        l1_guess[N,N] = 1.0
    elif ansatz == "minimalIC":
        l1_guess = 1e-3*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))
        l1_guess[:N,:N] = l1_old[:]
        l2_guess[:N,:N] = l2_old[:]
        l1_guess[N,N] = -1e-7*(-1)**N + 0.1j
        #l1_guess[N,N] = 1e+9
        #l1_guess[N,N] = (np.random.rand(1) + 1j*np.random.rand(1))
        #l2_guess[0,N] = (np.random.rand(1) + 1j*np.random.rand(1))
    elif ansatz == "mediumSym":
        l1_guess[:N,:N] = l1_old[:]
        l2_guess[:N,:N] = l2_old[:]
        if N <= 2:
            l1_guess[0,N]  = l1_guess[N,0] = 1e-4*(np.random.rand() + 1j*np.random.rand())
            l2_guess[0,N] =  l2_guess[N,0] = 1e-4*(np.random.rand() + 1j*np.random.rand()) 
        else:
            l1_guess[0,N]  = l1_guess[N,0] = 1*1e-5*(np.random.rand() + 1j*np.random.rand())
            l2_guess[0,N] =  l2_guess[N,0] = 1*1e-5*(np.random.rand() + 1j*np.random.rand()) 
        #l1_guess[N,N] = -1e-7*(-1)**N + 0.1j
        l1_guess[N,N] = -100 - 10j
        #l2_guess[N,N] = 1.0
    else:
        l1_guess[:N,:N] = l1_old[:]
        l2_guess[:N,:N] = l2_old[:]
        l1_guess[N,N] = -100-100j
        l1_guess[0,N] = l1_guess[N,0] = 10**(3/2)*(np.random.rand(1) + 1j*np.random.rand(1))
    return l1_guess,l2_guess


#################################################### 
# load solar models and compute potential
####################################################

rS,cS,rhoS,_,__ = np.loadtxt("../modelS_plusVAL_smooth.txt",unpack=True)
cS = cS 
rhoS = rhoS

ind = np.argsort(rS)
r = rS[ind]
c = cS[ind]
rho = rhoS[ind]
weight = np.ones(len(r))


'''
if atmospheric_model == "extension":
    print("loading extended model")
    rS,cS,rhoS,_,__,___ = np.loadtxt("../modelS-ext-further.txt",unpack=True)
    cS = (10**-2)*cS
    rhoS = (10**3)*rhoS
    ind = np.argsort(rS)
    r = rS[ind]
    c = cS[ind]
    rho = rhoS[ind]
    weight = np.ones(len(r))
'''



#if atmospheric_model == "extension":
if True:
    #r = rS[ind].tolist()
    #c = cS[ind].tolist()
    #rho = rhoS[ind].tolist() 
    #end_pt = 1.00375
   
    r = r.tolist()
    c = c.tolist()
    rho = rho.tolist()

    rho_clean = rho
    c_clean = c 
    
    r_beg = r[0]
    #r_end = r[-1]
    rho_beg = rho_clean[0]
    c_beg = c_clean[0]
    #rho_end = rho_clean[-1]
    #c_end = c_clean[-1]

    #print("c_clean[:-10] = ", c_clean[:-10])

    r_orig = r 
    c_orig = c

    r = [r_beg for i in range(spline_order+1)]  + r
    c_clean =  [c_beg for i in range(spline_order+1)] + c_clean 
    rho_clean =  [rho_beg for i in range(spline_order+1)] + rho_clean

    c_cleanBB = BSpline(spline_order,r,c_clean)
    rho_cleanBB = BSpline(spline_order,r,rho_clean)

    c_cleanB = c_cleanBB(x)
    rho_cleanB = rho_cleanBB(x)

    c_infty = c_cleanBB(a)
    alpha_infty = - rho_cleanBB.Differentiate()(a)/ rho_cleanBB(a) 
    rho_a = rho_cleanBB(a) 

    print("alpha_infty = ",  alpha_infty) 
    print("c_infty = ", c_infty)


    eval_c = [c_cleanBB(pp) for pp in  r_orig  ]
    eval_rho = [rho_cleanBB(pp) for pp in r_orig ]

    #plt.semilogy(r_orig, eval_c ,label="c-BSpline")
    #plt.plot(rS[ind] ,cS[ind],label="original")
    #plt.plot(r_orig ,c_orig,label="original")
    #plt.legend()
    #plt.show()
    
    #plt.semilogy(r_orig, eval_rho ,label="rho-BSpline")
    #plt.semilogy(rS[ind] ,rhoS[ind],label="original")
    #plt.legend()
    #plt.show()


print("Computing mesh nodes according to local wavelength")
mesh_r_pts = [] 
c_w = BSpline(spline_order,r,c_clean)
#omega_max_inner = RSun*53.0*10**(-3) 

#omega_max_inner = RSun*70.0*10**(-3) 
omega_max_inner = RSun*128.0*10**(-3) 

#omega_max_inner = RSun*45.0*10**(-3) 
omega_max_outer = RSun*128*10**(-3) 

def get_lambda_r(r_coord,omega_max):
    local_lambda = 2*pi*c_w(r_coord)/omega_max
    print("r = {0}, local_lambda = {1}".format(r_coord,local_lambda))
    return local_lambda
r_next = 1.0
#while r_next > 0.35:
while r_next > 0.15:
    local_lambda = get_lambda_r(r_next,omega_max_inner)
    r_next -= local_lambda
    mesh_r_pts.append(r_next)
mesh_r_pts.append(0.0)
mesh_r_pts = mesh_r_pts[::-1]

print("hello, mesh_r_pts = ", mesh_r_pts )

#mesh_r_pts.append(1.0) 
mesh_above_surface  = [] 
#if a == a_Atmo:
#    for ra_pt in a_list:
#        mesh_r_pts.append(ra_pt)
#else:
#    for ra_pt in a_list:
#        if ra_pt <= a:
#            mesh_r_pts.append(ra_pt)
for ra_pt in a_list:
    if ra_pt <= a:
        mesh_r_pts.append(ra_pt)
#if a > 1.0:
#    mesh_r_pts.append(a) 
mesh_pts_to_surface = mesh_r_pts.copy() 
#mesh_pts_to_surface.append(1.0) 
#mesh_above_surface = [a] 
for ra_pt in a_list:
    if ra_pt >= a:
        mesh_above_surface.append(ra_pt)
#r_next = 1.0
#
#while r_next < R_start_PML:
#    mesh_above_surface.append(r_next)
#    mesh_r_pts.append(r_next)
#    local_lambda = get_lambda_r(r_next,omega_max_inner)
#    r_next += local_lambda

#n_outer = 10
#start_point = a
#for i in range(1,n_outer+1):
#    new_point = start_point + (R_max_ODE - start_point)*i/n_outer
#    mesh_r_pts.append(new_point) 
#    mesh_above_surface.append(new_point)

print("mesh_r_pts = ", mesh_r_pts) 
print("len(mesh_r_pts) = ", len(mesh_r_pts)) 

print("mesh_pts up to transparent bndry = ", mesh_pts_to_surface) 
print("mesh_pts above transparent bndry = ", mesh_above_surface) 
 
#################################################### 
# damping model
####################################################

def get_damping(f_hz,lami=0):
   
    
    omega_f = f_hz*2*pi*RSun
    gamma0 = 2*pi*4.29*1e-6*RSun
    omega0 = 0.003*2*pi*RSun
    if f_hz < 0.0053:
        gamma = gamma0*(omega_f/omega0)**(5.77)
    else:
        gamma = gamma0*(0.0053*2*pi*RSun/omega0)**(5.77)

    #print("gamma_power = {0},gamma_fixed = {1}".format(gamma,2*pi*20*1e-6*RSun))
    #gamma = 2*pi*20*1e-6*RSun
    #gamma *= 1+ lami**0.35
    #gamma = gamma0*(0.003*2*pi*RSun/omega0)**(5.77)

    #if nodamping:
    #    gamma = 0
    #omega  = sqrt_2branch(1+2*1j*gamma)*omega_f
    gamma_min = 2*pi*1e-6*RSun
    if gamma < gamma_min:
        gamma = gamma_min
    
    omega_squared = omega_f**2+2*1j*omega_f*gamma

    #prefac = 1 + (  (omega_f/RSun-0.0033*2*pi)/( 0.5*0.0012*2*pi )   )**2
    #prefac = 1/prefac
    return sqrt_2branch(omega_squared),omega_squared

#################################################### 
# PML 
####################################################

def get_PML(f_hz):
    f0 = 10**3*f_hz
    #f0 = 10**3*0.003
    rad_min = R_start_PML
    rad_max = R_max_ODE
    C_PML = 100
    #C_PML = 0.01
    plm_min = rad_min
    w = rad_max-plm_min
    physpml_sigma = IfPos(x-plm_min,IfPos(rad_max -x,C_PML/w*(x-plm_min)**2/w**2,0),0)
    G_PML =  (1j/f0)*(C_PML/3)*(w/C_PML)**(3/2)*physpml_sigma**(3/2)
    pml_phys = pml.Custom(trafo=x+(1j/f0)*(C_PML/3)*(w/C_PML)**(3/2)*physpml_sigma**(3/2), jac = -1 - 1j*physpml_sigma/f0 )
    return pml_phys


#################################################### 
# Compute DtN numbers by solving ODEs
####################################################


DtN_continuous_eigvals = [] 
filtered_lam_all = []
DtN_ODE_filtered_all = [] 

#print("L_range = ", L_range)

lam_continuous = np.array( [l*(l+1)/a**2 for l in L_range ] ) 



def computed_learned_dtn(dtn_nr,Nrs,weight_type="ML"):

    step = 10
    L_range_filtered = L_range[::step]
    lam_continuous_filtered = lam_continuous[::step]

    weights_ML = np.array([10**6* filter_ell[l] for  l in L_range_filtered]) 
    weights_exp = np.array([10**6*np.exp(-l*alpha_decay) for l in L_range_filtered])


    if weight_type == "ML":
        Nrs_weight_type = Nrs 
    elif weight_type == "exp":
        Nrs_weight_type = [0] 
    else:
        raise ValueError('weight_type not accepted!')


    import ceres_dtn as opt
    result = {}
    A_N0 = []
    B_N0 = [] 
    for N in Nrs_weight_type: 
        result[N] = np.zeros(dtn_nr.shape,dtype="complex")

   
    for idx_f,f_hz in enumerate(f_hzs):
        print("f_hz = ", f_hz)
        dtn_ref = dtn_nr[idx_f,:]
        dtn_ref_filtered = dtn_ref[::step]
        #weights = np.array([10**6*np.exp(-l*alpha_decay) for l in L_range])
        #weights = np.array([10**6*np.exp(-l*alpha_decay) for l in L_range_filtered])
        #print("weights = ", weights) 
        #l_dtn = opt.learned_dtn(lam_continuous,dtn_ref,weights**2)
        
        if weight_type == "ML":
            weights =  weights_ML
        elif weight_type == "exp":
            weights = weights_exp 
        else:
            raise ValueError('weight_type not accepted!')

        l_dtn = opt.learned_dtn(lam_continuous_filtered,dtn_ref_filtered,weights**2)

        A_guess = np.random.rand(1,1) + 1j*np.random.rand(1,1)
        B_guess = np.random.rand(1,1) + 1j*np.random.rand(1,1)

        zeta_approx_evals = []

        A_IE = [ ]
        B_IE = [ ]
        
        #plt.plot(lam_continuous,dtn_ref.imag,label="ref",color="gray",linewidth=4)
        for N in Nrs_weight_type:
            l_dtn.Run(A_guess,B_guess,ansatz,flags)
            A_IE.append(A_guess.copy()), B_IE.append(B_guess.copy()) 
            zeta_learned = np.zeros(len(lam_continuous),dtype=complex)
            opt.eval_dtn_fct(A_IE[N],B_IE[N],np.array(lam_continuous,dtype=float),zeta_learned)
            (result[N])[idx_f,:] = zeta_learned[:]   
            A_guess,B_guess = new_initial_guess(A_IE[N],B_IE[N],ansatz)
        
            #plt.plot(lam_continuous,zeta_learned.imag,label="N={0}".format(N))
        #plt.legend()
        #plt.show() 
            #zeta_approx_evals.append(zeta_learned)
    for N in Nrs_weight_type:
        if weight_type == "ML":
            np.savetxt('data/dtn_nr_learned_N{0}-VALC-aHeight{1}-orig.out'.format(N,a_str_list[idx_a]),result[N],delimiter=',')
        elif weight_type == "exp":
                np.savetxt('data/dtn_nr_learned_N{0}-VALC-aHeight{1}-orig-exp-weights.out'.format(N,a_str_list[idx_a]),result[N],delimiter=',')
        else:
            raise ValueError('weight_type not accepted!')
    return result



def compute_power_spectrum_full(result,use_PML=True,use_conjugate=True):
    
    mesh_1D = Make1DMesh_givenpts(mesh_r_pts)
     
    mesh_pt_rtilde = mesh_1D(1.0)
    rho_surface_sq = sqrt(rho_cleanB(mesh_pt_rtilde))
    
    fes_DtN = H1(mesh_1D, complex=True,  order=order_ODE, dirichlet=[])
    u_DtN,v_DtN = fes_DtN.TnT()
    gfu_DtN = GridFunction (fes_DtN)

    f_DtN = LinearForm(fes_DtN)
    f_DtN.Assemble()
    r_DtN = f_DtN.vec.CreateVector()

    for i in range(fes_DtN.ndof):
        gfu_DtN.vec[i] = 1.0
        #f_DtN.vec[i] =  RSun*gfu_DtN(mesh_pt_rtilde)
        f_DtN.vec[i] =  gfu_DtN(mesh_pt_rtilde)
        gfu_DtN.vec[i] = 0.0

    for idx_f,f_hz in enumerate(f_hzs):

        omega, omega_squared = get_damping(f_hz)
        print("omega_squared = ", omega_squared)
        pot_1D = pot_rho_B - omega_squared/pot_c_B**2
         
        if use_PML:
            pml_phys = get_PML(f_hz)
            mesh_1D.SetPML(pml_phys,definedon=1)
        
        for idx_lami,lami in enumerate(lam_continuous):    
            
            a_DtN = BilinearForm (fes_DtN, symmetric=True)
            a_DtN += SymbolicBFI((x**2)*grad(u_DtN)*grad(v_DtN)) 
            a_DtN += SymbolicBFI(( lami.item() +  pot_1D*x**2 )*u_DtN*v_DtN)
            
            #a_DtN += SymbolicBFI((x**2/rho_cleanB)*grad(u_DtN)*grad(v_DtN)) 
            #a_DtN += SymbolicBFI(( lami.item()/rho_cleanB - omega_squared*x**2/(rho_cleanB*c_cleanB**2) )*u_DtN*v_DtN)
            
            a_DtN.Assemble()

            gfu_DtN.vec[:] = 0.0
            r_DtN.data = f_DtN.vec - a_DtN.mat * gfu_DtN.vec
            gfu_DtN.vec.data += a_DtN.mat.Inverse(freedofs=fes_DtN.FreeDofs(),inverse=solver) * r_DtN
             
            result[idx_f,idx_lami] = rho_surface_sq*gfu_DtN(mesh_pt_rtilde).imag
        
        if use_PML:
            mesh_1D.UnSetPML(1)

def compute_power_spectrum_dtn(result,dtn_nr,use_conjugate=True):
    print("Hello from compute_power_spectrum_dtn")
    #print("height a = ", a)
    print("mesh_pts_to_surface = ", mesh_pts_to_surface)
    mesh_1D = Make1DMesh_givenpts(mesh_pts_to_surface)
    mesh_pt_rtilde = mesh_1D(1.0)
    rho_surface_sq = sqrt(rho_cleanB(mesh_pt_rtilde))
    #mesh_pt_rtilde = mesh_1D(mesh_pts_to_surface[-1])
    #if a == a_Atmo:
    #    mesh_pt_rtilde_eps = mesh_1D(a-1e-12) # little shift to prevent evaluation exactly at end of Model S (problematic for BSpline approx.)
    #    rho_surface_sq = sqrt(rho_cleanB(mesh_pt_rtilde_eps))
    #    #print("rho_surface_sq = ", rho_surface_sq)  
    #else:
    #    rho_surface_sq = sqrt(rho_cleanB(mesh_pt_rtilde))
    #    #print("rho_surface_sq = ", rho_surface_sq)  

    
    fes_DtN = H1(mesh_1D, complex=True,  order=order_ODE, dirichlet=[])
    u_DtN,v_DtN = fes_DtN.TnT()
    gfu_DtN = GridFunction (fes_DtN)

    f_DtN = LinearForm(fes_DtN)
    f_DtN.Assemble()
    r_DtN = f_DtN.vec.CreateVector()

    for i in range(fes_DtN.ndof):
        gfu_DtN.vec[i] = 1.0
        f_DtN.vec[i] =  gfu_DtN(mesh_pt_rtilde)
        gfu_DtN.vec[i] = 0.0
    #print("f_DtN.vec =" , f_DtN.vec)

    for idx_f,f_hz in enumerate(f_hzs):
        print("idx_f =", idx_f)
        for idx_lami,lami in enumerate(lam_continuous):    
            omega, omega_squared = get_damping(f_hz,lami)
            #print("omega_squared = ", omega_squared)
            #pot_1D = pot_rho_B - omega_squared/pot_c_B**2
            
            a_DtN = BilinearForm (fes_DtN, symmetric=True)
            
            #a_DtN += SymbolicBFI((x**2)*grad(u_DtN)*grad(v_DtN)) 
            #a_DtN += SymbolicBFI(( lami.item() +  pot_1D*x**2 )*u_DtN*v_DtN)
            
            a_DtN += SymbolicBFI((x**2/rho_cleanB)*grad(u_DtN)*grad(v_DtN)) 
            a_DtN += SymbolicBFI(( a**2*lami.item()/rho_cleanB - omega_squared*x**2/(rho_cleanB*c_cleanB**2) )*u_DtN*v_DtN)
            #print(" dtn_nr[idx_f,idx_lami] = " , dtn_nr[idx_f,idx_lami])
            #a_DtN += SymbolicBFI((x**2/rho_cleanB)*dtn_nr[idx_f,idx_lami]*u_DtN*v_DtN,definedon=~mesh_1D.Boundaries(''))   
            
            #a_DtN += SymbolicBFI((x**2/rho_last_value)*dtn_nr[idx_f,idx_lami]*u_DtN*v_DtN,definedon=mesh_1D.Boundaries("right"))   
            a_DtN += SymbolicBFI((x**2/rho_cleanB)*dtn_nr[idx_f,idx_lami]*u_DtN*v_DtN,definedon=mesh_1D.Boundaries("right"))   
            #a_DtN += SymbolicBFI((x**2/rho_cleanB)*dtn_nr[idx_f,idx_lami]*u_DtN*v_DtN,definedon=mesh_1D.Boundaries("right"))   
            
            #a_DtN += SymbolicBFI(dtn_nr[idx_f,idx_lami]*u_DtN*v_DtN,definedon=mesh_1D.Boundaries("right"))   

            
            #a_DtN += SymbolicBFI(x**2*dtn_nr[idx_f,idx_lami]*u_DtN*v_DtN,definedon=mesh_1D.Boundaries("right"))   
            #a_DtN += SymbolicBFI((x**2/rho_cleanB)*dtn_nr[idx_f,idx_lami]*u_DtN*v_DtN,definedon=mesh_1D.Boundaries("right"))   
            a_DtN.Assemble()

            gfu_DtN.vec[:] = 0.0
            r_DtN.data = f_DtN.vec - a_DtN.mat * gfu_DtN.vec
            gfu_DtN.vec.data += a_DtN.mat.Inverse(freedofs=fes_DtN.FreeDofs(),inverse=solver) * r_DtN
            #print("gfu_DtN(mesh_pt_rtilde).imag = ", gfu_DtN(mesh_pt_rtilde).imag)
            #result[idx_f,idx_lami] = rho_surface_sq*gfu_DtN(mesh_pt_rtilde).imag
            result[idx_f,idx_lami] = gfu_DtN(mesh_pt_rtilde).imag
            #print("result[idx_f,idx_lami] = ", result[idx_f,idx_lami]) 
            #result[idx_f,idx_lami] = gfu_DtN(mesh_pt_rtilde).imag


def compute_power_spectrum_dirichlet(result,use_conjugate=True):
    print("Hello from compute_power_spectrum_dirichlet")
    #print("height a = ", a)
    print("mesh_pts_to_surface = ", mesh_pts_to_surface)
    mesh_1D = Make1DMesh_givenpts(mesh_pts_to_surface)
    mesh_pt_rtilde = mesh_1D(1.0)
    rho_surface_sq = sqrt(rho_cleanB(mesh_pt_rtilde))
    
    fes_DtN = H1(mesh_1D, complex=True,  order=order_ODE, dirichlet=[2])
    #fes_DtN = H1(mesh_1D, complex=True,  order=order_ODE, dirichlet=[])
    print("fes_DtN.FreeDofs() =  ", fes_DtN.FreeDofs())

    u_DtN,v_DtN = fes_DtN.TnT()
    gfu_DtN = GridFunction (fes_DtN)



    f_DtN = LinearForm(fes_DtN)
    f_DtN.Assemble()
    r_DtN = f_DtN.vec.CreateVector()

    for i in range(fes_DtN.ndof):
        gfu_DtN.vec[i] = 1.0
        f_DtN.vec[i] =  gfu_DtN(mesh_pt_rtilde)
        gfu_DtN.vec[i] = 0.0
    #print("f_DtN.vec =" , f_DtN.vec)

    for idx_f,f_hz in enumerate(f_hzs):
        print("idx_f =", idx_f)
        for idx_lami,lami in enumerate(lam_continuous):    
            omega, omega_squared = get_damping(f_hz,lami)
            #print("omega_squared = ", omega_squared)
            #pot_1D = pot_rho_B - omega_squared/pot_c_B**2
            
            a_DtN = BilinearForm (fes_DtN, symmetric=True)
            #a_DtN += SymbolicBFI((x**2)*grad(u_DtN)*grad(v_DtN)) 
            #a_DtN += SymbolicBFI(( lami.item() +  pot_1D*x**2 )*u_DtN*v_DtN)


            a_DtN += SymbolicBFI((x**2/rho_cleanB)*grad(u_DtN)*grad(v_DtN)) 
            a_DtN += SymbolicBFI(( a**2*lami.item()/rho_cleanB - omega_squared*x**2/(rho_cleanB*c_cleanB**2) )*u_DtN*v_DtN)
            #a_DtN += SymbolicBFI((x**2/rho_cleanB)*dtn_nr[idx_f,idx_lami]*u_DtN*v_DtN,definedon=mesh_1D.Boundaries("right"))   

            #a_DtN += SymbolicBFI((x**2/rho_cleanB)*grad(u_DtN)*grad(v_DtN)) 
            #a_DtN += SymbolicBFI(( lami.item()/rho_cleanB - omega_squared*x**2/(rho_cleanB*c_cleanB**2) )*u_DtN*v_DtN)
            #a_DtN += SymbolicBFI(dtn_nr[idx_f,idx_lami]*u_DtN*v_DtN,definedon=mesh_1D.Boundaries("right"))   
            #a_DtN += SymbolicBFI(x**2*dtn_nr[idx_f,idx_lami]*u_DtN*v_DtN,definedon=mesh_1D.Boundaries("right"))   
            #a_DtN += SymbolicBFI((x**2/rho_cleanB)*dtn_nr[idx_f,idx_lami]*u_DtN*v_DtN,definedon=mesh_1D.Boundaries("right"))   
            a_DtN.Assemble()

            gfu_DtN.vec[:] = 0.0
            r_DtN.data = f_DtN.vec - a_DtN.mat * gfu_DtN.vec
            gfu_DtN.vec.data += a_DtN.mat.Inverse(freedofs=fes_DtN.FreeDofs(),inverse=solver) * r_DtN
            #print("gfu_DtN(mesh_pt_rtilde).imag = ", gfu_DtN(mesh_pt_rtilde).imag)
            #result[idx_f,idx_lami] = rho_surface_sq*gfu_DtN(mesh_pt_rtilde).imag
            result[idx_f,idx_lami] = gfu_DtN(mesh_pt_rtilde).imag
            #print("result[idx_f,idx_lami] = ", result[idx_f,idx_lami]) 
            #result[idx_f,idx_lami] = gfu_DtN(mesh_pt_rtilde).imag


def compute_power_spectrum_dirichlet_ext(result,use_conjugate=True):
    print("Hello from compute_power_spectrum_dirichlet")
    #print("height a = ", a)
    print("mesh_pts_to_surface = ", mesh_pts_to_surface)
    
    #end_pt = 1.0035881999999816
    #end_pt = 1.00325
    
    #end_pt = 1.005 #power-spectrum-Atmo-DirichletBC-ext-orig-r1p005-3480km.out
    #end_pt = 1.0025 #power-spectrum-Atmo-DirichletBC-ext-origr1p0025-1740km.out
    #end_pt = 1.001437 
    end_pt = 1.001078
    dxw = mesh_pts_to_surface[-1]-mesh_pts_to_surface[-2] 
    pt_current = mesh_pts_to_surface[-1] + dxw 
    while( pt_current < end_pt): 
        mesh_pts_to_surface.append(pt_current) 
        pt_current += dxw  

    print("mesh_pts_to_surface = ", mesh_pts_to_surface)

    mesh_1D = Make1DMesh_givenpts(mesh_pts_to_surface)
    mesh_pt_rtilde = mesh_1D(1.0)
    rho_surface_sq = sqrt(rho_cleanB(mesh_pt_rtilde))
    
    fes_DtN = H1(mesh_1D, complex=True,  order=order_ODE, dirichlet=[2])
    #fes_DtN = H1(mesh_1D, complex=True,  order=order_ODE, dirichlet=[])
    print("fes_DtN.FreeDofs() =  ", fes_DtN.FreeDofs())

    u_DtN,v_DtN = fes_DtN.TnT()
    gfu_DtN = GridFunction (fes_DtN)



    f_DtN = LinearForm(fes_DtN)
    f_DtN.Assemble()
    r_DtN = f_DtN.vec.CreateVector()

    for i in range(fes_DtN.ndof):
        gfu_DtN.vec[i] = 1.0
        f_DtN.vec[i] =  gfu_DtN(mesh_pt_rtilde)
        gfu_DtN.vec[i] = 0.0
    #print("f_DtN.vec =" , f_DtN.vec)

    for idx_f,f_hz in enumerate(f_hzs):
        print("idx_f =", idx_f)
        for idx_lami,lami in enumerate(lam_continuous):    
            omega, omega_squared = get_damping(f_hz,lami)
            #print("omega_squared = ", omega_squared)
            #pot_1D = pot_rho_B - omega_squared/pot_c_B**2
            
            a_DtN = BilinearForm (fes_DtN, symmetric=True)
            #a_DtN += SymbolicBFI((x**2)*grad(u_DtN)*grad(v_DtN)) 
            #a_DtN += SymbolicBFI(( lami.item() +  pot_1D*x**2 )*u_DtN*v_DtN)


            a_DtN += SymbolicBFI((x**2/rho_cleanB)*grad(u_DtN)*grad(v_DtN)) 
            a_DtN += SymbolicBFI(( a**2*lami.item()/rho_cleanB - omega_squared*x**2/(rho_cleanB*c_cleanB**2) )*u_DtN*v_DtN)
            #a_DtN += SymbolicBFI((x**2/rho_cleanB)*dtn_nr[idx_f,idx_lami]*u_DtN*v_DtN,definedon=mesh_1D.Boundaries("right"))   

            #a_DtN += SymbolicBFI((x**2/rho_cleanB)*grad(u_DtN)*grad(v_DtN)) 
            #a_DtN += SymbolicBFI(( lami.item()/rho_cleanB - omega_squared*x**2/(rho_cleanB*c_cleanB**2) )*u_DtN*v_DtN)
            #a_DtN += SymbolicBFI(dtn_nr[idx_f,idx_lami]*u_DtN*v_DtN,definedon=mesh_1D.Boundaries("right"))   
            #a_DtN += SymbolicBFI(x**2*dtn_nr[idx_f,idx_lami]*u_DtN*v_DtN,definedon=mesh_1D.Boundaries("right"))   
            #a_DtN += SymbolicBFI((x**2/rho_cleanB)*dtn_nr[idx_f,idx_lami]*u_DtN*v_DtN,definedon=mesh_1D.Boundaries("right"))   
            a_DtN.Assemble()

            gfu_DtN.vec[:] = 0.0
            r_DtN.data = f_DtN.vec - a_DtN.mat * gfu_DtN.vec
            gfu_DtN.vec.data += a_DtN.mat.Inverse(freedofs=fes_DtN.FreeDofs(),inverse=solver) * r_DtN
            #print("gfu_DtN(mesh_pt_rtilde).imag = ", gfu_DtN(mesh_pt_rtilde).imag)
            #result[idx_f,idx_lami] = rho_surface_sq*gfu_DtN(mesh_pt_rtilde).imag
            result[idx_f,idx_lami] = gfu_DtN(mesh_pt_rtilde).imag
            #print("result[idx_f,idx_lami] = ", result[idx_f,idx_lami]) 
            #result[idx_f,idx_lami] = gfu_DtN(mesh_pt_rtilde).imag


def get_ODE_DtN_Dirichlet(dtn_nr,use_PML):

    #alpha_infty = 6633.460440647692
    #c_infty = 6855.398433326527
    #rho_cleanB =  rho_a * exp(-alpha_infty*(x-a))
    #c_cleanB = c_infty

    mesh_1D = Make1DMesh_givenpts(mesh_above_surface)  
    fes_DtN = H1(mesh_1D, complex=True,  order=order_ODE, dirichlet=[1,2])
    u_DtN,v_DtN = fes_DtN.TnT()
    gfu_DtN = GridFunction (fes_DtN)

    f_DtN = LinearForm(fes_DtN)
    f_DtN.Assemble()
    r_DtN = f_DtN.vec.CreateVector()
    frees_DtN = [i for i in range(1,fes_DtN.ndof)]

    for idx_f,f_hz in enumerate(f_hzs):
        print("f_hz = ", f_hz)
                
        if use_PML:
            pml_phys = get_PML(f_hz)
            mesh_1D.SetPML(pml_phys,definedon=1)
        
        for idx_lami,lami in enumerate(lam_continuous):    
        
            omega, omega_squared = get_damping(f_hz,lami)
            #pot_1D = pot_rho_B - omega_squared/pot_c_B**2
            
            a_DtN = BilinearForm (fes_DtN, symmetric=False)
            a_DtN += SymbolicBFI((x**2/rho_cleanB)*grad(u_DtN)*grad(v_DtN)) 
            a_DtN += SymbolicBFI(( a**2*lami.item()/rho_cleanB - omega_squared*x**2/(rho_cleanB*c_cleanB**2) )*u_DtN*v_DtN)
            a_DtN.Assemble()

            gfu_DtN.vec[:] = 0.0
            gfu_DtN.Set(1.0, definedon=mesh_1D.Boundaries("left"))
            r_DtN.data = f_DtN.vec - a_DtN.mat * gfu_DtN.vec
            gfu_DtN.vec.data += a_DtN.mat.Inverse(freedofs=fes_DtN.FreeDofs(),inverse=solver) * r_DtN
           
            rows_DtN,cols_DtN,vals_DtN = a_DtN.mat.COO()
            A_DtN = csr_matrix((vals_DtN,(rows_DtN,cols_DtN)))
            val = -P_DoFs(A_DtN,[0],frees_DtN).dot(gfu_DtN.vec.FV().NumPy()[frees_DtN])-P_DoFs(A_DtN,[0],[0]).dot(gfu_DtN.vec.FV().NumPy()[0])

            dtn_nr[idx_f,idx_lami] =  -rho_a*val.item()/a**2 

        if use_PML:
            mesh_1D.UnSetPML(1)

    np.savetxt('data/dtn_nr_VALC-aHeight{0}-orig.out'.format(a_str_list[idx_a]),dtn_nr,delimiter=',')



dtn_nr = np.zeros((len(f_hzs),len(L_range)),dtype="complex")
#loaded_dtn_nr = np.zeros((len(f_hzs),len(L_range)),dtype="complex")
power_spectrum_dtn = np.zeros((len(f_hzs),len(L_range)))
power_spectrum = np.zeros((len(f_hzs),len(L_range)))

if dtn_approx == "exact":
    try:
        dtn_nr_VALC = np.loadtxt('data/dtn_nr_VALC-aHeight{0}-orig.out'.format(a_str_list[idx_a]),dtype="complex",delimiter=',')
    except:
        dtn_nr_VALC = np.zeros((len(f_hzs),len(L_range)),dtype="complex")
        get_ODE_DtN_Dirichlet(dtn_nr_VALC,use_PML)
    with TaskManager():
        compute_power_spectrum_dtn(power_spectrum, dtn_nr_VALC)
    np.savetxt('data/Greens-fct-imag-exact.out',power_spectrum,delimiter=',')
    #np.savetxt('power-spectrum-nonlocal.out',power_spectrum,delimiter=',')
if dtn_approx == "only_learning":
    try:
        dtn_nr_VALC = np.loadtxt('data/dtn_nr_VALC-aHeight{0}-orig.out'.format(a_str_list[idx_a]),dtype="complex",delimiter=',')
    except:
        dtn_nr_VALC = np.zeros((len(f_hzs),len(L_range)),dtype="complex")
        get_ODE_DtN_Dirichlet(dtn_nr_VALC,use_PML)
        dtn_nr_VALC = np.loadtxt('data/dtn_nr_VALC-aHeight{0}-orig.out'.format(a_str_list[idx_a]),dtype="complex",delimiter=',')
    if dtn_approx == "learned-exp":
        learned_dtn_nrs_exp = computed_learned_dtn(dtn_nr_VALC,Nrs,weight_type="exp")
    learned_dtn_nrs = computed_learned_dtn(dtn_nr_VALC,Nrs,weight_type="ML")
if dtn_approx == "learned":
    for N in [N_compute]:
        try:
            #dtn_nr_learned = np.loadtxt('dtn_nr_learned_N{0}-Atmo-aHeight{1}.out'.format(N,a_str_list[idx_a]),dtype="complex",delimiter=',')
            dtn_nr_learned = np.loadtxt('data/dtn_nr_learned_N{0}-VALC-aHeight{1}-orig.out'.format(N,a_str_list[idx_a]),dtype="complex",delimiter=',')
        except:
            raise ValueError('File dtn_nr_learned_N{0}-VALC-aHeight{1}-orig.out not found!'.format(N,a_str_list[idx_a]))
            #try:
            #    dtn_nr = np.loadtxt('dtn_nr_Whitw-comp.out',dtype="complex",delimiter=',')
            #except:
            #    get_atmo_dtn(dtn_nr)
            #learned_dtn_nrs = computed_learned_dtn(dtn_nr,Nrs)
            #dtn_nr_learned = np.loadtxt('dtn_nr_learned_N{0}-Atmo.out'.format(N),dtype="complex",delimiter=',') 
        with TaskManager():
            compute_power_spectrum_dtn( power_spectrum, dtn_nr_learned )
        np.savetxt('data/Greens-fct-imag-N{0}.out'.format(N,a_str_list[idx_a]),power_spectrum,delimiter=',')
if dtn_approx == "learned-exp":
    for N in [N_compute]:
        try:
            #dtn_nr_learned = np.loadtxt('dtn_nr_learned_N{0}-Atmo-aHeight{1}.out'.format(N,a_str_list[idx_a]),dtype="complex",delimiter=',')
            dtn_nr_learned = np.loadtxt('data/dtn_nr_learned_N{0}-VALC-aHeight{1}-orig-exp-weights.out'.format(N,a_str_list[idx_a]),dtype="complex",delimiter=',')
        except:
            raise ValueError('File dtn_nr_learned_N{0}-VALC-aHeight{1}-orig-exp-weights.out not found!'.format(N,a_str_list[idx_a]))
            #try:
            #    dtn_nr = np.loadtxt('dtn_nr_Whitw-comp.out',dtype="complex",delimiter=',')
            #except:
            #    get_atmo_dtn(dtn_nr)
            #learned_dtn_nrs = computed_learned_dtn(dtn_nr,Nrs)
            #dtn_nr_learned = np.loadtxt('dtn_nr_learned_N{0}-Atmo.out'.format(N),dtype="complex",delimiter=',') 
        with TaskManager():
            compute_power_spectrum_dtn( power_spectrum, dtn_nr_learned )
        np.savetxt('data/Greens-fct-imag-N{0}-expweights.out'.format(N),power_spectrum,delimiter=',')




