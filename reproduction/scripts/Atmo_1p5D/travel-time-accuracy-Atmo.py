from math import pi
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from scipy.integrate import simps 
import urllib.request

import sys,os
file_dir = os.path.dirname(os.path.realpath(__file__))
upfolder_path=  os.path.abspath(os.path.join( os.path.dirname( __file__ ), '..' ))
sys.path.append(upfolder_path)
from time_distance_helio import compute_TD_diagram, normalize_C_comp,filter_ell,PrecomputeFactors

Nmax = 3
generate_plots_for_check = False 
alternative_filter = True
only_comp_required_theta = True

N_start_fac = {3.0: 55/300, 
            4.0: 60/300,
            5.2: 60/300,
            6.0: 60/300,
            7.0: 60/300,
            8.0: 60/300,
            }
N_stop_fac = {3.0: 110/300,
            4.0: 110/300,
            5.2: 110/300,
            6.0: 110/300,
            7.0: 110/300,
            8.0: 110/300,
            }

inp_freq = 8.0
inp_str =  "3p0"

#power_MJ = np.loadtxt('../powerl_MJ.txt')
#ell_MJ = np.arange(len(power_MJ))
#print("ell_MJ = ", ell_MJ)
#ell_MJe = np.arange(len(power_MJ)+1)
#coef_MJ = np.polyfit(ell_MJ,power_MJ,4)
#filter_ell = np.polyval(coef_MJ,ell_MJe)
#plt.figure()
#plt.plot(ell_MJ, power_MJ)
#plt.plot(ell_MJe,filter_ell)
#plt.show()


plt.rc('legend', fontsize=18)
plt.rc('axes', titlesize=18)
plt.rc('axes', labelsize=18)
plt.rc('xtick', labelsize=14)    
plt.rc('ytick', labelsize=14)

# load power spectra 
power_spectrum_ref = np.loadtxt('data/Greens-fct-imag-exact.out',delimiter=',')
power_spectra_approx = [  ]
power_spectra_approx.append(np.loadtxt('data/Greens-fct-imag-Dirichlet-bc.out',delimiter=',')) 
power_spectra_approx.append(np.loadtxt('data/Greens-fct-imag-SAI1.out',delimiter=',')) 
power_spectra_approx.append(np.loadtxt('data/Greens-fct-imag-SHF1a.out',delimiter=',')) 
power_spectra_approx.append(np.loadtxt('data/Greens-fct-imag-N0.out',delimiter=',')) 
power_spectra_approx.append(np.loadtxt('data/Greens-fct-imag-N1.out',delimiter=',')) 
power_spectra_approx.append(np.loadtxt('data/Greens-fct-imag-N2.out',delimiter=',')) 

f_hz_comp = 1e-3*2*pi*np.linspace(0,8.3,power_spectrum_ref.shape[0])
f_hz = f_hz_comp
f_hz_plot = f_hz_comp*10**3/(2*pi)
print("f_hz_plot = ", f_hz_plot)   
L_range = np.arange(0,power_spectrum_ref.shape[1])

print("power_spectrum_ref.shape[1]", power_spectrum_ref.shape[1])

N_approx = ["N{0}".format(N) for N in range(Nmax)]
power_approx_str = ["Dirichlet","SAI-1","SHF1a"] + N_approx 
dtn_approx_str = ["SAI-1","SHF1a"] + N_approx 

# load dtn numbers 
dtn_ref = np.loadtxt('data/dtn_nr_Whitw-comp-orig.out',dtype="complex",delimiter=',')
dtn_approx = [ ]

idx_1mHz =  np.argmin(np.abs(  f_hz_plot - 1.0 ) )
idx_3p0mHz =  np.argmin(np.abs(  f_hz_plot - 3.0 ) )
idx_3p5mHz =  np.argmin(np.abs(  f_hz_plot - 3.5 ) )
idx_5p2mHz =  np.argmin(np.abs(  f_hz_plot - 5.2 ) )
idx_6p0mHz =  np.argmin(np.abs(  f_hz_plot - 6.0 ) )
idx_8p0mHz =  np.argmin(np.abs(  f_hz_plot - 8.0 ) )
print("idx_3p5mHz = ",  f_hz_plot[idx_3p5mHz] )  
print("idx_5p2mHz = ",  f_hz_plot[idx_5p2mHz] )  
print("idx_8p0mHz = ",  f_hz_plot[idx_8p0mHz] )  
dtn_approx.append( np.loadtxt('data/dtn_nr_SAI-1-aHeight510km.out',dtype="complex",delimiter=','))
dtn_approx.append( np.loadtxt('data/dtn_nr_S-HF-1a.out',dtype="complex",delimiter=','))
dtn_approx.append( np.loadtxt('data/dtn_nr_learned_N0-Atmo-aHeight510km.out',dtype="complex",delimiter=','))
dtn_approx.append( np.loadtxt('data/dtn_nr_learned_N1-Atmo-aHeight510km.out',dtype="complex",delimiter=','))
dtn_approx.append( np.loadtxt('data/dtn_nr_learned_N2-Atmo-aHeight510km.out',dtype="complex",delimiter=','))


idx_mHz = [idx_3p0mHz,idx_3p5mHz, idx_5p2mHz, idx_6p0mHz, idx_8p0mHz ]
idx_str = ["3p0mHz","3p5mHz","5p2mHz", "6p0mHz","8p0mHz" ]
for idx_m,str_m in zip(idx_mHz,idx_str): 

    fname = "Atmo-dtn-imag-{0}.dat".format(str_m)
    vals_header = "l exact" 
    plot_collect = [ np.array(L_range,dtype="float"), dtn_ref[idx_m,:].imag ] 
    
    for dtn_app,str_app in zip( dtn_approx, dtn_approx_str):
        vals_header += " {0}".format(str_app)
        plot_collect.append(dtn_app.imag[idx_m,:]  )

    np.savetxt(fname = fname,
                   X = np.transpose(plot_collect),
                   header = vals_header,
                   comments = '')
    
    if generate_plots_for_check: 
        print("idx_m = ", idx_m)
        plt.plot(L_range,dtn_ref[idx_m,:].imag,label="ref",linewidth=3,color="gray")
        for dtn_app,str_app in zip( dtn_approx, dtn_approx_str): 
            plt.plot(L_range,dtn_app[idx_m,:].imag,label="{0}".format(str_app),linestyle="dashed")
        plt.title("Imaginary part, {0}".format( str_m ) )
        plt.legend() 
        plt.show() 


aHeight = "0km"
freq_spec = "custom"

#def scale_power_spectrum(power):
#
#    #return power
#    def PI_scal(f):
#        result = 1 + ((abs(f)-3.3*1e-3)/ (0.6*1e-3) )**2
#        return 1/result 
#    
#    scaled_power = np.zeros(power.shape) 
#    for i  in range(power.shape[0]):
#        if abs(f_hz[i]) > 1e-13:
#            scaled_power[i,:] = power[i,:]*0.5*PI_scal(f_hz[i])/(2*pi*f_hz[i])
#        else:
#            scaled_power[i,:] = power[i,:]*0.5*PI_scal(f_hz[i])
#    return scaled_power

def scale_power_spectrum(power):

    #return power
    def PI_scal(f):
        result = 1 + ((abs(f)-3.3*1e-3)/ (0.6*1e-3) )**2
        return 1/result 
    
    scaled_power = np.zeros(power.shape) 
    for i  in range(power.shape[0]):
        for l in range(power.shape[1]):
            if abs(f_hz[i]) > 1e-13:
                scaled_power[i,l] = power[i,l]*filter_ell[l]*0.5*PI_scal(f_hz[i])/(2*pi*f_hz[i])
            else:
                scaled_power[i,l] = power[i,l]*filter_ell[l]*0.5*PI_scal(f_hz[i])
    return scaled_power



scaled_power_Atmo = scale_power_spectrum( power_spectrum_ref )
scaled_power_approx = [] 
for ps in power_spectra_approx: 
    scaled_power_approx.append( scale_power_spectrum(ps) ) 


print(np.max( scaled_power_Atmo ))
 
power_spectrum = scaled_power_Atmo /np.max( scaled_power_Atmo ) 
vmin = 2.0e-4
im = plt.pcolormesh(L_range,f_hz_plot[idx_1mHz:], power_spectrum[idx_1mHz:,:],
                   cmap=plt.get_cmap('viridis'),
                   norm=colors.LogNorm( vmin= vmin ,vmax=1.0  ) )
                   #cmap=plt.get_cmap('Reds'),

plt.rc('legend', fontsize=16)
plt.rc('axes', titlesize=16)
plt.rc('axes', labelsize=16)
plt.xlabel("Harmonic degree $\ell$")
#plt.colorbar(im)
plt.gcf().set_size_inches(6, 7)
plt.savefig("power-spectrum-Atmo.png",transparent=True)
if generate_plots_for_check: 
    plt.show()
plt.close()

for idx_m,str_m in zip(idx_mHz,idx_str): 

    fname = "Atmo-power-{0}.dat".format(str_m)
    fname_error = "Atmo-power-error-{0}.dat".format(str_m)
    vals_header = "l exact" 
    errors_header = "l " 
    plot_collect = [ np.array(L_range,dtype="float"), scaled_power_Atmo[idx_m,:] ] 
    errors_collect = [ np.array(L_range,dtype="float")  ] 

    for ps_app,str_app in zip( scaled_power_approx, power_approx_str ):
        vals_header += " {0}".format(str_app)
        errors_header += " {0}".format(str_app)
        plot_collect.append(ps_app[idx_m,:] )
        errors_collect.append( np.abs(  (ps_app[idx_m,:] - scaled_power_Atmo[idx_m,:] )/ scaled_power_Atmo[idx_m,:] ) )

    np.savetxt(fname = fname,
                   X = np.transpose(plot_collect),
                   header = vals_header,
                   comments = '')

    np.savetxt(fname = fname_error,
                   X = np.transpose(errors_collect),
                   header = errors_header,
                   comments = '')
    
    if generate_plots_for_check: 
        print("idx_m = ", idx_m)
        plt.plot(L_range,  scaled_power_Atmo[idx_m,:], label="ref",linewidth=3,color="gray")    
        for ps_app,str_app in zip( scaled_power_approx, power_approx_str ):
            plt.plot(L_range, ps_app[idx_m,:],label="{0}".format(str_app),linestyle="dashed")
        plt.title("Power spectrum part, {0}".format( str_m ) )
        plt.legend() 
        plt.show() 

        for err,err_str in zip( errors_collect[1:] , power_approx_str ):
            plt.semilogy(L_range, err, label="{0}".format(err_str) )
        plt.title("Relative error, {0}".format( str_m ) )
        plt.legend() 
        plt.show() 

if generate_plots_for_check: 

    for ps_str,ps in zip( power_approx_str ,power_spectra_approx): 

        fig = plt.figure(figsize=(6,5),constrained_layout=True)
        diff = np.abs(  (  power_spectrum_ref -  ps  ) /  (  power_spectrum_ref  ) )
        diff[diff < 1e-13] = 1e-13
        vmin = 1e-3
        im = plt.pcolormesh(L_range,f_hz_plot,diff,
                                   cmap=plt.get_cmap('jet'),
                                   norm=colors.LogNorm( vmin= vmin ,vmax= 1e0  ) )
        plt.xlabel("Harmonic degree $\ell$",labelpad=-2)
        plt.ylabel("Frequency (mHz)")
        plt.colorbar(im)
        plt.title("Relative error for {0}".format(ps_str))
        plt.show()
        plt.cla()
        plt.clf()
        plt.close()


#generate_plots_for_check = True 

Nt = 2**13
#Nt = 2**13
N_theta = 1000
thetas = np.linspace(0,pi,N_theta)

N_test = 30
theta_test = [ pi/12 + (i/N_test)*pi*(1/6 - 1/12) for i in range(N_test+1)   ]


theta_test = [ pi/6 ]
theta_str = "30deg"

idxx = [  np.argmin(np.abs(thetas - theta_t ) ) for theta_t in theta_test  ]
idxx_label = ["{0}deg ".format( theta_t*180/pi ) for theta_t in theta_test ]
max_vals = [1.0 for i in range(len(idxx ))]


ts = np.linspace(0,300,Nt)
tss =  60*ts/(2*pi)
domega = f_hz[-1]/power_spectrum_ref.shape[0]
dt = tss[-1]/(Nt)
dt_phys = 2*pi*dt
domega_phys = 1/(dt_phys)

Ntmp = int(1/(domega*dt))

ts_minutes = ts 
ts_seconds = 60*ts 
min2seconds = 60

print("Ntmp = ", Ntmp)
print("dt_phys  = ", dt_phys )
print(" domega_phys  = ", domega_phys  )



travel_time_perturb = []

input_freqs = [3.0,4.0,5.2,6.0,7.0,8.0]
input_freqs_str = ["3p0","4p0","5p2","6p0","7p0","8p0"]

#input_freqs = [4.0]
#input_freqs_str = ["4p0"]

for inp_freq,inp_str in zip( input_freqs,input_freqs_str):
    print("Computing for {0} mHz".format(inp_str))
    precomputed_factors = PrecomputeFactors( power_spectrum = power_spectrum_ref, inp_freq=inp_freq , f_hz=f_hz)
    
    C_t, C_partial_t,  W_tau, weights_t, normalizations   = compute_TD_diagram( power_spectrum = power_spectrum_ref, inp_freq=inp_freq , f_hz=f_hz,name_str="time-distance-ref-a0km",only_omega=True,W_tau=True, idxx=idxx, idxx_label=idxx_label,  N_start_fac=N_start_fac, N_stop_fac= N_stop_fac, only_comp_required_theta = only_comp_required_theta,precomputed_factors = precomputed_factors )

    C_t_approx = [] 
    for ps_app,str_app in zip( power_spectra_approx, power_approx_str ):
        C_t_approx.append(compute_TD_diagram( power_spectrum = ps_app, inp_freq=inp_freq , f_hz=f_hz, name_str="time-distance-Atmo-approx",idxx=idxx, idxx_label=idxx_label, N_start_fac=N_start_fac, N_stop_fac= N_stop_fac, only_comp_required_theta = only_comp_required_theta,precomputed_factors = precomputed_factors  )) 

    for i in range(len(C_t_approx)): 
        normalize_C_comp( C_t_approx[i] , normalizations, thetas )

    theta_sample = np.array( [  thetas[j] for j in idxx ] )
    approx_delta_taus = [] 

    j0 = idxx[0]
    cross_header = "t window Atmo " 
    cross_abserr_header = "t " 
    plot_collect_cross  = [ts,weights_t.real[:,j0],C_t.real[:,j0] ]
    plot_collect_abserr  = [ts  ]

    for ps_str,CtA in zip(power_approx_str, C_t_approx): 

        print("Computing for {0}".format(ps_str))
        approx_delta_tau = [    ]

        for j in idxx:
            print("thetha =", thetas[j]) 
            
            diffs = []
            diffs_noweights = []

            exact_travel_time =  min2seconds*simps( W_tau[:,j]*(C_t[:,j]),x=ts_minutes ) 
            print(" exact_travel_time = {0}".format( exact_travel_time  ))
 
            # commpute travel time perturbation
            diff_approx = min2seconds * simps( W_tau[:,j]*(C_t[:,j] - CtA[:,j]),x=ts_minutes  )
            approx_delta_tau.append(abs( diff_approx)) 

            if j == j0: 

                if generate_plots_for_check: 
                    plt.plot(ts,C_t.real[:,j]  ,label="C_Atmo" )
                    plt.plot(ts,CtA.real[:,j]  ,label="C {0}".format(ps_str))
                    plt.plot(ts,weights_t.real[:,j]  ,label="weights")
                    plt.legend()
                    plt.show()
 
                plot_collect_cross.append( CtA.real[:,j] ) 
                cross_header += "{0} ".format(ps_str) 
                plot_collect_abserr.append( np.abs( C_t.real[:,j] -  CtA.real[:,j]) ) 
                cross_abserr_header  += "{0} ".format(ps_str) 

        approx_delta_taus.append( approx_delta_tau[0] )  
    
    travel_time_perturb.append( approx_delta_taus )

    if generate_plots_for_check: 
        plt.plot( plot_collect_cross[0], plot_collect_cross[1] , label="weights", color='b')
        plt.plot( plot_collect_cross[0], plot_collect_cross[2] , label="Atmo")
        for ps_str, cross_app in zip(power_approx_str, plot_collect_cross[3:]):
            plt.plot( plot_collect_cross[0], cross_app, label="{0}".format( ps_str) )
        plt.legend()
        plt.show()
    
    np.savetxt(fname = "cross-comp-Atmo-f{0}.dat".format(inp_str), 
                   X = np.transpose(plot_collect_cross),
                   header = cross_header,
                   comments = '')

    np.savetxt(fname = "cross-abserr-Atmo-f{0}.dat".format(inp_str), 
                   X = np.transpose( plot_collect_abserr),
                   header = cross_abserr_header  ,
                   comments = '')

dtau_header = "f0"
dtau_collect = [input_freqs]  
for idx, method_str in enumerate(power_approx_str):
    dtau_header += " "
    dtau_header += method_str 
    method_dtau = []
    for i in range(len(input_freqs)):
        method_dtau.append(  travel_time_perturb[i][idx])
    dtau_collect.append(method_dtau)

np.savetxt(fname = "dtau-Atmo-theta{0}.dat".format( theta_str  ), 
               X = np.transpose(  dtau_collect ),
               header = dtau_header ,
               comments = '')

