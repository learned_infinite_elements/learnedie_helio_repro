from netgen.meshing import *
from netgen.csg import *
from netgen.meshing import Mesh as netmesh
from math import pi,ceil
import numpy as np
import matplotlib.pyplot as plt
from scipy.sparse import csr_matrix
from ngsolve import Mesh as NGSMesh

import sys,os
file_dir = os.path.dirname(os.path.realpath(__file__))
upfolder_path=  os.path.abspath(os.path.join( os.path.dirname( __file__ ), '..' ))
sys.path.append(upfolder_path)
from time_distance_helio import filter_ell

from cmath import sqrt as csqrt
np.random.seed(seed=1)
from ngsolve import *
ngsglobals.msg_level = 0
SetNumThreads(8)

#################################################### 
# parameters
####################################################


atmospheric_models = ["VALC","Atmo","extension"]
#atmospheric_models = ["VALC","Atmo"]
atmospheric_model = atmospheric_models[2]
dtn_approximations = ["exact","learned","nonlocal","SHF1a","SAI0","SAI1","AHF1","ARBC1"]
N_compute = 0

if ( len(sys.argv) > 1 and sys.argv[1] in dtn_approximations + ["only_learning","exact","Dirichlet","Dirichlet-ext","Dirichlet-dtn"]  ):
    dtn_approx = sys.argv[1]
    idx_a = int(sys.argv[2])
else:
    raise ValueError('I do not know this atmospheric Model!')

if ( dtn_approx ==  "learned"):
    if (len(sys.argv) > 2 and int(sys.argv[2]) in range(10) ):
        N_compute = int(sys.argv[2])
        if (len(sys.argv) > 3 and int(sys.argv[3]) in range(6)):
                idx_a = int(sys.argv[3])
        else:
            raise ValueError("Invalid input for learned IE computation.") 
    else:
        raise ValueError("Invalid input for learned IE computation.") 

#dtn_approx = dtn_approximations[6]

order_ODE = 5 # order of ODE discretization
L_min = 0 
L_max = 1000
L_range = np.arange(L_min,L_max+1)
spline_order = order_ODE # order of Spline approx. for coefficients
R_max_ODE = 1.0036 # to how far out the ODE for VAL-C model should be solved

# depracated 
R_start_PML = 1.001
R_min_ODE = 0.0

prec = 3000

use_PML = False

end_pt_Dirichlet = 1.05 #
km_str = "34800km"


RSun =  6.96*10**8
#a_Atmo = 1.0007126
a_Atmo = 1.000731467456897006
#a_list = [1.0, 1.0 + 0.5*(a_Atmo-1.0),a_Atmo]
#N_outer = 5
N_outer = 5
a_list = [  1.0 + (i/N_outer)*(a_Atmo-1.0) for i in range(N_outer+1)  ]
a_str_list = ["{0}km".format( int(ceil( (aval-1.0)*RSun/1000)) ) for aval in a_list]
print("a_str_list = ", a_str_list)

if atmospheric_model == "VALC": 
    a = 1.0 # radius of trucation boundary
else:
    a = a_list[idx_a]
    #if dtn_approx in ["learned","only_learning","exact"]: 
    #    a = a_list[idx_a]
    #else:
    #    print("Setting transparent b.c. at 500km above surface since Atmospheric Radiation B.C. are used.")
    #    a = a_Atmo
print("a = {0}".format(a))

Nmax = 3 # maximal dofs for learned IE in radial direction
alpha_decay = 1/60 # exponential decay factor of weights
Nrs = list(range(Nmax))
show_plots = True
#solver = "pardiso"
solver = "sparsecholesky"
ansatz = "mediumSym"
flags = {"max_num_iterations":10000,
         "check_gradients":False, 
         "use_nonmonotonic_steps":True,
         "minimizer_progress_to_stdout":False,
         "num_threads":12,
         "report_level":"Brief",
         "function_tolerance":1e-12,
         "parameter_tolerance":1e-12,
         "gradient_tolerance":1e-12}

nodamping  = False


#step = 10
#L_range_filtered = L_range[::step]
#weights_exp = np.array([10**6*np.exp(-l*alpha_decay) for l in L_range_filtered])
#weights_ML = np.array([10**6* filter_ell[l] for  l in L_range_filtered])
#plt.plot( L_range_filtered,weights_exp,label="exp" )
#plt.plot( L_range_filtered,weights_ML,label="from power" )
#plt.xlabel("$\ell$")
#plt.legend()
#plt.show()
#input("")

f_hzs = np.linspace(0,8.3,7200)
f_hzs = (10**-3)*f_hzs

#################################################### 
# some auxiliary functions
####################################################

def sqrt_2branch(z):
    if z.real < 0 and z.imag <0:
        return - csqrt(z)
    else:
        return csqrt(z)

def P_DoFs(M,test,basis):
    return (M[test,:][:,basis])


def Make1DMesh_givenpts(mesh_r_pts):

    mesh_1D = netmesh(dim=1)
    pids = []   
    for r_coord in mesh_r_pts:
        pids.append (mesh_1D.Add (MeshPoint(Pnt(r_coord, 0, 0))))                     
    n_mesh = len(pids)-1
    for i in range(n_mesh):
        mesh_1D.Add(Element1D([pids[i],pids[i+1]],index=1))
    mesh_1D.Add (Element0D( pids[0], index=1))
    mesh_1D.Add (Element0D( pids[n_mesh], index=2))
    mesh_1D.SetBCName(0,"left")
    mesh_1D.SetBCName(1,"right")
    mesh_1D = NGSMesh(mesh_1D)    
    return mesh_1D

def new_initial_guess(l1_old,l2_old,ansatz):
    N = l1_old.shape[0]
    l1_guess = np.zeros((N+1,N+1),dtype='complex')
    l2_guess = np.zeros((N+1,N+1),dtype='complex')
    if ansatz in ["medium","full"]:
        l1_guess = 1e-3*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))
        l2_guess = 1e-3*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))
        l1_guess[:N,:N] = l1_old[:]
        l2_guess[:N,:N] = l2_old[:]
        l1_guess[N,N] = 1.0
    elif ansatz == "minimalIC":
        l1_guess = 1e-3*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))
        l1_guess[:N,:N] = l1_old[:]
        l2_guess[:N,:N] = l2_old[:]
        l1_guess[N,N] = -1e-7*(-1)**N + 0.1j
        #l1_guess[N,N] = 1e+9
        #l1_guess[N,N] = (np.random.rand(1) + 1j*np.random.rand(1))
        #l2_guess[0,N] = (np.random.rand(1) + 1j*np.random.rand(1))
    elif ansatz == "mediumSym":
        l1_guess[:N,:N] = l1_old[:]
        l2_guess[:N,:N] = l2_old[:]
        if N <= 2:
            l1_guess[0,N]  = l1_guess[N,0] = 1e-4*(np.random.rand() + 1j*np.random.rand())
            l2_guess[0,N] =  l2_guess[N,0] = 1e-4*(np.random.rand() + 1j*np.random.rand()) 
        else:
            l1_guess[0,N]  = l1_guess[N,0] = 1*1e-5*(np.random.rand() + 1j*np.random.rand())
            l2_guess[0,N] =  l2_guess[N,0] = 1*1e-5*(np.random.rand() + 1j*np.random.rand()) 
        #l1_guess[N,N] = -1e-7*(-1)**N + 0.1j
        l1_guess[N,N] = -100 - 10j
        #l2_guess[N,N] = 1.0
    else:
        l1_guess[:N,:N] = l1_old[:]
        l2_guess[:N,:N] = l2_old[:]
        l1_guess[N,N] = -100-100j
        l1_guess[0,N] = l1_guess[N,0] = 10**(3/2)*(np.random.rand(1) + 1j*np.random.rand(1))
    return l1_guess,l2_guess


#################################################### 
# load solar models and compute potential
####################################################

rS,cS,rhoS,_,__ = np.loadtxt("../modelS_Bspline_EXT_expRho.txt",unpack=True)
cS = cS 
rhoS = rhoS

if atmospheric_model == "VALC": 
    print("using Model S + VAL-C")
    rV,vV,rhoV,Tv  = np.loadtxt("../modelS_plusVAL_smooth.txt",unpack=True)

    # sound speed is not given in VAL-C model
    # calculate it from temperature using the ideal gas law
    gamma_ideal = 5/3 # adiabatic index
    mu_ideal = 1.3*10**(-3) # mean molecular weight 
    R_gas = 8.31446261815324 # universal gas constant 
    cV = np.sqrt(gamma_ideal*R_gas*Tv/mu_ideal)
    rhoV = (10**3)*rhoV

    # overlap interval
    rmin = rV[-1]
    rmax = rS[0]

    weightS = np.minimum(1, (rmax-rS)/(rmax-rmin))
    weightV = np.minimum(1, (rV-rmin)/(rmax-rmin))

    r = np.concatenate((rS,rV)) 
    ind = np.argsort(r)
    r = r[ind]
    c = np.concatenate((cS,cV))[ind]
    rho = np.concatenate((rhoS,rhoV))[ind]
    weight = np.concatenate((weightS,weightV))[ind]
else:
    print("using Model S + Atmo")
    ind = np.argsort(rS)
    r = rS[ind]
    c = cS[ind]
    rho = rhoS[ind]
    weight = np.ones(len(r))

idx1 = np.argmin(np.abs(r - a))
idx2 = idx1 + 5

r = r[:idx2+1]
c = c[:idx2+1]
rho = rho[:idx2+1]

print("r = ", r[-10:] )

rho_prime_end =  (rho[idx1]   - rho[idx1-1] )/ (r[idx1] - r[idx1-1] )
alpha_infty = -rho_prime_end /  rho[idx1] 
c_infty =  c[idx1]

print("alpha_infty = ",  alpha_infty) 
print("c_infty = ", c_infty)



'''
if atmospheric_model == "extension":
    print("loading extended model")
    rS,cS,rhoS,_,__,___ = np.loadtxt("../modelS-ext-further.txt",unpack=True)
    cS = (10**-2)*cS
    rhoS = (10**3)*rhoS
    ind = np.argsort(rS)
    r = rS[ind]
    c = cS[ind]
    rho = rhoS[ind]
    weight = np.ones(len(r))
'''

c0 = 6.855e5 # sound speed at interface [cm/s]
H = 1.25e7 # density scale height in [cm]
omega_c = 0.0052*2*pi # c0/(2*H) cut-off frequency in Herz

#if atmospheric_model == "extension":
if True:
   
    r = r.tolist()
    c = c.tolist()
    rho = rho.tolist()

    rho_clean = rho
    c_clean = c 
    
    r_beg = r[0]
    #r_end = r[-1]
    rho_beg = rho_clean[0]
    c_beg = c_clean[0]

    r_orig = r 
    c_orig = c

    r = [r_beg for i in range(spline_order+1)]  + r
    c_clean =  [c_beg for i in range(spline_order+1)] + c_clean 
    rho_clean =  [rho_beg for i in range(spline_order+1)] + rho_clean

    c_cleanBB = BSpline(spline_order,r,c_clean)
    rho_cleanBB = BSpline(spline_order,r,rho_clean)

    c_cleanB = c_cleanBB(x)
    rho_cleanB = rho_cleanBB(x)

    c_infty = c_cleanBB(a)
    alpha_infty = - rho_cleanBB.Differentiate()(a)/ rho_cleanBB(a) 
    rho_a = rho_cleanBB(a) 

    print("alpha_infty = ",  alpha_infty) 
    print("c_infty = ", c_infty)


    eval_c = [c_cleanBB(pp) for pp in  r_orig  ]
    eval_rho = [rho_cleanBB(pp) for pp in r_orig ]

    #plt.semilogy(r_orig, eval_c ,label="c-BSpline")
    #plt.plot(rS[ind] ,cS[ind],label="original")
    #plt.plot(r_orig ,c_orig,label="original")
    #plt.legend()
    #plt.show()
    
    #plt.semilogy(r_orig, eval_rho ,label="rho-BSpline")
    #plt.semilogy(rS[ind] ,rhoS[ind],label="original")
    #plt.legend()
    #plt.show()



print("Computing mesh nodes according to local wavelength")
mesh_r_pts = [] 
c_w = BSpline(spline_order,r,c_clean)
#omega_max_inner = RSun*53.0*10**(-3) 

#omega_max_inner = RSun*70.0*10**(-3) 
omega_max_inner = RSun*128.0*10**(-3) 

#omega_max_inner = RSun*45.0*10**(-3) 
omega_max_outer = RSun*128*10**(-3) 

def get_lambda_r(r_coord,omega_max):
    local_lambda = 2*pi*c_w(r_coord)/omega_max
    print("r = {0}, local_lambda = {1}".format(r_coord,local_lambda))
    return local_lambda
r_next = 1.0
#while r_next > 0.35:
while r_next > 0.15:
    local_lambda = get_lambda_r(r_next,omega_max_inner)
    r_next -= local_lambda
    mesh_r_pts.append(r_next)
mesh_r_pts.append(0.0)
mesh_r_pts = mesh_r_pts[::-1]

print("hello, mesh_r_pts = ", mesh_r_pts )

#mesh_r_pts.append(1.0) 
mesh_above_surface  = [] 
#if a == a_Atmo:
#    for ra_pt in a_list:
#        mesh_r_pts.append(ra_pt)
#else:
#    for ra_pt in a_list:
#        if ra_pt <= a:
#            mesh_r_pts.append(ra_pt)
for ra_pt in a_list:
    if ra_pt <= a:
        mesh_r_pts.append(ra_pt)
#if a > 1.0:
#    mesh_r_pts.append(a) 
mesh_pts_to_surface = mesh_r_pts.copy() 
#mesh_pts_to_surface.append(1.0) 
#mesh_above_surface = [a] 
for ra_pt in a_list:
    if ra_pt >= a:
        mesh_above_surface.append(ra_pt)
#r_next = 1.0
#
#while r_next < R_start_PML:
#    mesh_above_surface.append(r_next)
#    mesh_r_pts.append(r_next)
#    local_lambda = get_lambda_r(r_next,omega_max_inner)
#    r_next += local_lambda

#n_outer = 10
#start_point = a
#for i in range(1,n_outer+1):
#    new_point = start_point + (R_max_ODE - start_point)*i/n_outer
#    mesh_r_pts.append(new_point) 
#    mesh_above_surface.append(new_point)

print("mesh_r_pts = ", mesh_r_pts) 
print("len(mesh_r_pts) = ", len(mesh_r_pts)) 

print("mesh_pts up to transparent bndry = ", mesh_pts_to_surface) 
print("mesh_pts above transparent bndry = ", mesh_above_surface) 
 
#################################################### 
# damping model
####################################################

def get_damping(f_hz,lami=0):
   
    
    omega_f = f_hz*2*pi*RSun
    gamma0 = 2*pi*4.29*1e-6*RSun
    omega0 = 0.003*2*pi*RSun
    if f_hz < 0.0053:
        gamma = gamma0*(omega_f/omega0)**(5.77)
    else:
        gamma = gamma0*(0.0053*2*pi*RSun/omega0)**(5.77)

    #print("gamma_power = {0},gamma_fixed = {1}".format(gamma,2*pi*20*1e-6*RSun))
    #gamma = 2*pi*20*1e-6*RSun
    #gamma *= 1+ lami**0.35
    #gamma = gamma0*(0.003*2*pi*RSun/omega0)**(5.77)

    #if nodamping:
    #    gamma = 0
    #omega  = sqrt_2branch(1+2*1j*gamma)*omega_f
    gamma_min = 2*pi*1e-6*RSun
    if gamma < gamma_min:
        gamma = gamma_min
    
    omega_squared = omega_f**2+2*1j*omega_f*gamma

    #prefac = 1 + (  (omega_f/RSun-0.0033*2*pi)/( 0.5*0.0012*2*pi )   )**2
    #prefac = 1/prefac
    return sqrt_2branch(omega_squared),omega_squared

#################################################### 
# PML 
####################################################

def get_PML(f_hz):
    f0 = 10**3*f_hz
    #f0 = 10**3*0.003
    rad_min = R_start_PML
    rad_max = R_max_ODE
    C_PML = 100
    #C_PML = 0.01
    plm_min = rad_min
    w = rad_max-plm_min
    physpml_sigma = IfPos(x-plm_min,IfPos(rad_max -x,C_PML/w*(x-plm_min)**2/w**2,0),0)
    G_PML =  (1j/f0)*(C_PML/3)*(w/C_PML)**(3/2)*physpml_sigma**(3/2)
    pml_phys = pml.Custom(trafo=x+(1j/f0)*(C_PML/3)*(w/C_PML)**(3/2)*physpml_sigma**(3/2), jac = -1 - 1j*physpml_sigma/f0 )
    return pml_phys


#################################################### 
# Compute DtN numbers by solving ODEs
####################################################


DtN_continuous_eigvals = [] 
filtered_lam_all = []
DtN_ODE_filtered_all = [] 

#print("L_range = ", L_range)

lam_continuous = np.array( [l*(l+1)/a**2 for l in L_range ] ) 

def get_ODE_DtN(dtn_nr,dtn_nr_whitw,use_PML):
    
    print("Hello from get_ODE_DtN")
    if len(mesh_above_surface) == 1:
        dtn_nr = dtn_nr_whitw
    else: 
        mesh_1D = Make1DMesh_givenpts(mesh_above_surface)  
        fes_DtN = H1(mesh_1D, complex=True,  order=order_ODE, dirichlet=[1])
        u_DtN,v_DtN = fes_DtN.TnT()
        gfu_DtN = GridFunction (fes_DtN)

        f_DtN = LinearForm(fes_DtN)
        f_DtN.Assemble()
        r_DtN = f_DtN.vec.CreateVector()
        frees_DtN = [i for i in range(1,fes_DtN.ndof)]

        for idx_f,f_hz in enumerate(f_hzs):
            print("f_hz = ", f_hz)

                    
            if use_PML:
                pml_phys = get_PML(f_hz)
                mesh_1D.SetPML(pml_phys,definedon=1)
            
            for idx_lami,lami in enumerate(lam_continuous):    
            
                omega, omega_squared = get_damping(f_hz,lami)
                pot_1D = pot_rho_B - omega_squared/pot_c_B**2
                
                a_DtN = BilinearForm (fes_DtN, symmetric=False)
                a_DtN += SymbolicBFI((x**2)*grad(u_DtN)*grad(v_DtN)) 
                a_DtN += SymbolicBFI(( lami.item()*a**2 +  pot_1D*x**2 )*u_DtN*v_DtN)
                a_DtN += SymbolicBFI(x**2*dtn_nr_whitw[idx_f,idx_lami]*u_DtN*v_DtN,definedon=mesh_1D.Boundaries("right"))   
                a_DtN.Assemble()

                gfu_DtN.vec[:] = 0.0
                gfu_DtN.Set(1.0, BND)
                r_DtN.data = f_DtN.vec - a_DtN.mat * gfu_DtN.vec
                gfu_DtN.vec.data += a_DtN.mat.Inverse(freedofs=fes_DtN.FreeDofs(),inverse=solver) * r_DtN
               
                rows_DtN,cols_DtN,vals_DtN = a_DtN.mat.COO()
                A_DtN = csr_matrix((vals_DtN,(rows_DtN,cols_DtN)))
                val = -P_DoFs(A_DtN,[0],frees_DtN).dot(gfu_DtN.vec.FV().NumPy()[frees_DtN])-P_DoFs(A_DtN,[0],[0]).dot(gfu_DtN.vec.FV().NumPy()[0])

                dtn_nr[idx_f,idx_lami] =  -val.item()/a**2 

            if use_PML:
                mesh_1D.UnSetPML(1)

    np.savetxt('data/ODE_dtn_nr-aHeight{0}.out'.format(a_str_list[idx_a]),dtn_nr,delimiter=',')



def get_atmo_dtn(dtn_nr):
    print("Getting Atmo dtn numbers from whittaker function")
    from ngs_arb import Whitw_deriv_over_Whitw,Whitw_deriv_over_Whitw_par,Whitw_deriv_over_Whitw_spar

    for idx_f,f_hz in enumerate(f_hzs):
        print("idx = {0}, f_hz = {1}".format(idx_f,f_hz))
        sigma, sigma_squared = get_damping(f_hz)
        k_squared = sigma_squared/c_infty**2 - 0.25*alpha_infty**2
        k_omega = sqrt_2branch(k_squared)
        eta = alpha_infty/(2*k_omega)
        chi = -1j*eta
        z_arg = -2*1j*k_omega*a
        #kappas_l = [chi for l in L_range ]
        #mus_l = [l+0.5 for l in L_range ]
        #zs_l = [z_arg for l in L_range ]
        #wh_qout =  np.array(Whitw_deriv_over_Whitw( kappas_l,mus_l,zs_l,prec)) 
        #wh_qout =  np.array(Whitw_deriv_over_Whitw_par( kappas_l,mus_l,zs_l,prec)) 
        
        wh_qout =  np.array(Whitw_deriv_over_Whitw_spar(chi,z_arg,L_max,prec)) 
        
        DtN_atmo =  1/a + 0.5*alpha_infty + 2*1j*k_omega*wh_qout
        #DtN_atmo = 1/a + 0.5*alpha_infty + 2*1j*k_omega*wh_qout
        print("dtn ", DtN_atmo[:-10])
        dtn_nr[idx_f,:] = DtN_atmo[:]
        #if idx_f == 5:
        #plt.plot(L_range,DtN_atmo.real)
        #plt.show() 
        #plt.plot(L_range,DtN_atmo.imag)
        #plt.show()
    np.savetxt('data/dtn_nr_Whitw-comp-orig.out',dtn_nr,delimiter=',')


def get_atmo_nonlocal(dtn_nr):
    for idx_f,f_hz in enumerate(f_hzs):
        print("idx = {0}, f_hz = {1}".format(idx_f,f_hz))
        sigma, sigma_squared = get_damping(f_hz)
        k_squared = sigma_squared/c_infty**2 - 0.25*alpha_infty**2
        k_omega = sqrt_2branch(k_squared)
        #DtN_atmo = [ -1j*csqrt(sigma_squared/c_infty**2 - 0.25*alpha_infty**2 - alpha_infty/a - l*(l+1)/a**2 )  for l in L_range]
        DtN_atmo = [ 1/a - 1j*k_omega*csqrt( 1 - alpha_infty/(a*k_squared)  - l*(l+1)/(a**2*k_squared) )  for l in L_range]
        dtn_nr[idx_f,:] = DtN_atmo[:]
    np.savetxt('data/dtn_nr_nonlocal.out',dtn_nr,delimiter=',')

def get_atmo_SHF1a(dtn_nr):
    #print("alpha_infty =", alpha_infty)
    #print("c_infty =", c_infty)
    #print("a =", a)

    for idx_f,f_hz in enumerate(f_hzs):
        print("idx = {0}, f_hz = {1}".format(idx_f,f_hz))
        sigma, sigma_squared = get_damping(f_hz)
        k_squared = sigma_squared/c_infty**2 - 0.25*alpha_infty**2
        k_omega = sqrt_2branch(k_squared)
        DtN_atmo = [ 1/a + alpha_infty/2  - 1j*k_omega + (1j/(2*k_omega))*alpha_infty/a  for l in L_range]
        dtn_nr[idx_f,:] = DtN_atmo[:]
    np.savetxt('data/dtn_nr_S-HF-1a.out',dtn_nr,delimiter=',')

def get_atmo_SAI0(dtn_nr):
    for idx_f,f_hz in enumerate(f_hzs):
        print("idx = {0}, f_hz = {1}".format(idx_f,f_hz))
        sigma, sigma_squared = get_damping(f_hz)
        k_squared = sigma_squared/c_infty**2 - 0.25*alpha_infty**2
        k_omega = sqrt_2branch(k_squared)
        DtN_atmo = [ 1/a + alpha_infty/2  -1j*k_omega*csqrt( 1- alpha_infty/(a*k_squared) )  for l in L_range]
        dtn_nr[idx_f,:] = DtN_atmo[:]
    np.savetxt('data/dtn_nr_SAI-0.out',dtn_nr,delimiter=',')

def get_atmo_SAI1(dtn_nr):
    for idx_f,f_hz in enumerate(f_hzs):
        print("idx = {0}, f_hz = {1}".format(idx_f,f_hz))
        sigma, sigma_squared = get_damping(f_hz)
        k_squared = sigma_squared/c_infty**2 - 0.25*alpha_infty**2
        k_omega = sqrt_2branch(k_squared)
        DtN_atmo = [ 1/a +  alpha_infty/2   -1j*k_omega*csqrt( 1- alpha_infty/(a*k_squared) ) + 0.5*1j* ( (l*(l+1))/(a**2*k_omega ))/ csqrt(1 - (alpha_infty/(a*k_squared) ) )  for l in L_range]
        dtn_nr[idx_f,:] = DtN_atmo[:]
    np.savetxt('data/dtn_nr_SAI-1-aHeight{0}.out'.format(a_str_list[idx_a]),dtn_nr,delimiter=',')

def get_atmo_AHF1(dtn_nr):
    for idx_f,f_hz in enumerate(f_hzs):
        print("idx = {0}, f_hz = {1}".format(idx_f,f_hz))
        sigma, sigma_squared = get_damping(f_hz)
        k_squared = sigma_squared/c_infty**2 - 0.25*alpha_infty**2
        k_omega = sqrt_2branch(k_squared)
        DtN_atmo = [ 1/a +  alpha_infty/2  - 1j*sigma/c_infty - (c_infty/(2*1j*sigma))*( l*(l+1)/a**2 + (alpha_infty/a) + 0.25*alpha_infty**2 )   for l in L_range]
        dtn_nr[idx_f,:] = DtN_atmo[:]
    np.savetxt('data/dtn_nr_AHF1.out',dtn_nr,delimiter=',')

#def get_atmo_ARBC1(dtn_nr):
#    for idx_f,f_hz in enumerate(f_hzs):
#        print("idx = {0}, f_hz = {1}".format(idx_f,f_hz))
#        sigma, sigma_squared = get_damping(f_hz)
#        k_squared = sigma_squared/c_infty**2 - 0.25*alpha_infty**2
#        k_omega = sqrt_2branch(k_squared)
#        DtN_atmo = [ 1/a - 1/a - 1j*k_omega for l in L_range]
#        dtn_nr[idx_f,:] = DtN_atmo[:]
#    np.savetxt('dtn_nr_ARBC1.out',dtn_nr,delimiter=',')

def computed_learned_dtn(dtn_nr,Nrs):

    step = 10
    L_range_filtered = L_range[::step]
    lam_continuous_filtered = lam_continuous[::step]

    weights_ML = np.array([10**6* filter_ell[l] for  l in L_range_filtered])


    import ceres_dtn as opt
    result = {}
    for N in Nrs:
        result[N] = np.zeros(dtn_nr.shape,dtype="complex")
   
    for idx_f,f_hz in enumerate(f_hzs):
        print("f_hz = ", f_hz)
        dtn_ref = dtn_nr[idx_f,:]
        dtn_ref_filtered = dtn_ref[::step]
        #weights = np.array([10**6*np.exp(-l*alpha_decay) for l in L_range])
        
        weights =  weights_ML 
        #weights = np.array([10**6*np.exp(-l*alpha_decay) for l in L_range_filtered])
        #print("weights = ", weights) 
        
        #l_dtn = opt.learned_dtn(lam_continuous,dtn_ref,weights**2)
        l_dtn = opt.learned_dtn(lam_continuous_filtered,dtn_ref_filtered,weights**2)

        A_guess = np.random.rand(1,1) + 1j*np.random.rand(1,1)
        B_guess = np.random.rand(1,1) + 1j*np.random.rand(1,1)

        zeta_approx_evals = []

        A_IE = []
        B_IE = []
        
        #plt.plot(lam_continuous,dtn_ref.imag,label="ref",color="gray",linewidth=4)
        for N in Nrs:
            l_dtn.Run(A_guess,B_guess,ansatz,flags)
            A_IE.append(A_guess.copy()), B_IE.append(B_guess.copy()) 
            zeta_learned = np.zeros(len(lam_continuous),dtype=complex)
            opt.eval_dtn_fct(A_IE[N],B_IE[N],np.array(lam_continuous,dtype=float),zeta_learned)
            (result[N])[idx_f,:] = zeta_learned[:]   
            A_guess,B_guess = new_initial_guess(A_IE[N],B_IE[N],ansatz)
        
            #plt.plot(lam_continuous,zeta_learned.imag,label="N={0}".format(N))
        #plt.legend()
        #plt.show() 
            #zeta_approx_evals.append(zeta_learned)
    for N in Nrs:
        if atmospheric_model == "VALC": 
            np.savetxt('data/dtn_nr_learned_N{0}-VALC.out'.format(N),result[N],delimiter=',')
        else:
            np.savetxt('data/dtn_nr_learned_N{0}-Atmo-aHeight{1}.out'.format(N,a_str_list[idx_a]),result[N],delimiter=',')
    return result



def compute_power_spectrum_full(result,use_PML=True,use_conjugate=True):
    
    mesh_1D = Make1DMesh_givenpts(mesh_r_pts)
    
    #NN = 500000
    #bpts = [j*R_max_ODE/NN for j in range(NN+1) ]
    
    #sample_bpts = [mesh_1D(bpt) for bpt in mesh_r_pts]
    #sample_bpts = [mesh_1D(bpt) for bpt in bpts]
    #c_bpts = [c_cleanB(sbpt) for sbpt in sample_bpts] 

    #plt.plot( mesh_r_pts ,c_bpts)
    #plt.plot(bpts,c_bpts)
    #plt.show()
    
    mesh_pt_rtilde = mesh_1D(1.0)
    rho_surface_sq = sqrt(rho_cleanB(mesh_pt_rtilde))
    
    fes_DtN = H1(mesh_1D, complex=True,  order=order_ODE, dirichlet=[])
    u_DtN,v_DtN = fes_DtN.TnT()
    gfu_DtN = GridFunction (fes_DtN)

    f_DtN = LinearForm(fes_DtN)
    f_DtN.Assemble()
    r_DtN = f_DtN.vec.CreateVector()

    for i in range(fes_DtN.ndof):
        gfu_DtN.vec[i] = 1.0
        #f_DtN.vec[i] =  RSun*gfu_DtN(mesh_pt_rtilde)
        f_DtN.vec[i] =  gfu_DtN(mesh_pt_rtilde)
        gfu_DtN.vec[i] = 0.0

    for idx_f,f_hz in enumerate(f_hzs):

        omega, omega_squared = get_damping(f_hz)
        print("omega_squared = ", omega_squared)
        pot_1D = pot_rho_B - omega_squared/pot_c_B**2
         
        if use_PML:
            pml_phys = get_PML(f_hz)
            mesh_1D.SetPML(pml_phys,definedon=1)
        
        for idx_lami,lami in enumerate(lam_continuous):    
            
            a_DtN = BilinearForm (fes_DtN, symmetric=True)
            a_DtN += SymbolicBFI((x**2)*grad(u_DtN)*grad(v_DtN)) 
            a_DtN += SymbolicBFI(( lami.item() +  pot_1D*x**2 )*u_DtN*v_DtN)
            
            #a_DtN += SymbolicBFI((x**2/rho_cleanB)*grad(u_DtN)*grad(v_DtN)) 
            #a_DtN += SymbolicBFI(( lami.item()/rho_cleanB - omega_squared*x**2/(rho_cleanB*c_cleanB**2) )*u_DtN*v_DtN)
            
            a_DtN.Assemble()

            gfu_DtN.vec[:] = 0.0
            r_DtN.data = f_DtN.vec - a_DtN.mat * gfu_DtN.vec
            gfu_DtN.vec.data += a_DtN.mat.Inverse(freedofs=fes_DtN.FreeDofs(),inverse=solver) * r_DtN
             
            result[idx_f,idx_lami] = rho_surface_sq*gfu_DtN(mesh_pt_rtilde).imag
        
        if use_PML:
            mesh_1D.UnSetPML(1)

def compute_power_spectrum_dtn(result,dtn_nr,use_conjugate=True):
    print("Hello from compute_power_spectrum_dtn")
    #print("height a = ", a)
    print("mesh_pts_to_surface = ", mesh_pts_to_surface)
    mesh_1D = Make1DMesh_givenpts(mesh_pts_to_surface)
    mesh_pt_rtilde = mesh_1D(1.0)
    rho_surface_sq = sqrt(rho_cleanB(mesh_pt_rtilde))
    #mesh_pt_rtilde = mesh_1D(mesh_pts_to_surface[-1])
    #if a == a_Atmo:
    #    mesh_pt_rtilde_eps = mesh_1D(a-1e-12) # little shift to prevent evaluation exactly at end of Model S (problematic for BSpline approx.)
    #    rho_surface_sq = sqrt(rho_cleanB(mesh_pt_rtilde_eps))
    #    #print("rho_surface_sq = ", rho_surface_sq)  
    #else:
    #    rho_surface_sq = sqrt(rho_cleanB(mesh_pt_rtilde))
    #    #print("rho_surface_sq = ", rho_surface_sq)  

    
    fes_DtN = H1(mesh_1D, complex=True,  order=order_ODE, dirichlet=[])
    u_DtN,v_DtN = fes_DtN.TnT()
    gfu_DtN = GridFunction (fes_DtN)

    f_DtN = LinearForm(fes_DtN)
    f_DtN.Assemble()
    r_DtN = f_DtN.vec.CreateVector()

    for i in range(fes_DtN.ndof):
        gfu_DtN.vec[i] = 1.0
        f_DtN.vec[i] =  gfu_DtN(mesh_pt_rtilde)
        gfu_DtN.vec[i] = 0.0
    #print("f_DtN.vec =" , f_DtN.vec)

    for idx_f,f_hz in enumerate(f_hzs):
        print("idx_f =", idx_f)
        for idx_lami,lami in enumerate(lam_continuous):    
            omega, omega_squared = get_damping(f_hz,lami)
            #print("omega_squared = ", omega_squared)
            #pot_1D = pot_rho_B - omega_squared/pot_c_B**2
            
            a_DtN = BilinearForm (fes_DtN, symmetric=True)
            
            #a_DtN += SymbolicBFI((x**2)*grad(u_DtN)*grad(v_DtN)) 
            #a_DtN += SymbolicBFI(( lami.item() +  pot_1D*x**2 )*u_DtN*v_DtN)
            
            a_DtN += SymbolicBFI((x**2/rho_cleanB)*grad(u_DtN)*grad(v_DtN)) 
            a_DtN += SymbolicBFI(( a**2*lami.item()/rho_cleanB - omega_squared*x**2/(rho_cleanB*c_cleanB**2) )*u_DtN*v_DtN)
            #print(" dtn_nr[idx_f,idx_lami] = " , dtn_nr[idx_f,idx_lami])
            #a_DtN += SymbolicBFI((x**2/rho_cleanB)*dtn_nr[idx_f,idx_lami]*u_DtN*v_DtN,definedon=~mesh_1D.Boundaries(''))   
            
            #a_DtN += SymbolicBFI((x**2/rho_last_value)*dtn_nr[idx_f,idx_lami]*u_DtN*v_DtN,definedon=mesh_1D.Boundaries("right"))   
            a_DtN += SymbolicBFI((x**2/rho_cleanB)*dtn_nr[idx_f,idx_lami]*u_DtN*v_DtN,definedon=mesh_1D.Boundaries("right"))   
            #a_DtN += SymbolicBFI((x**2/rho_cleanB)*dtn_nr[idx_f,idx_lami]*u_DtN*v_DtN,definedon=mesh_1D.Boundaries("right"))   
            
            #a_DtN += SymbolicBFI(dtn_nr[idx_f,idx_lami]*u_DtN*v_DtN,definedon=mesh_1D.Boundaries("right"))   

            
            #a_DtN += SymbolicBFI(x**2*dtn_nr[idx_f,idx_lami]*u_DtN*v_DtN,definedon=mesh_1D.Boundaries("right"))   
            #a_DtN += SymbolicBFI((x**2/rho_cleanB)*dtn_nr[idx_f,idx_lami]*u_DtN*v_DtN,definedon=mesh_1D.Boundaries("right"))   
            a_DtN.Assemble()

            gfu_DtN.vec[:] = 0.0
            r_DtN.data = f_DtN.vec - a_DtN.mat * gfu_DtN.vec
            gfu_DtN.vec.data += a_DtN.mat.Inverse(freedofs=fes_DtN.FreeDofs(),inverse=solver) * r_DtN
            #print("gfu_DtN(mesh_pt_rtilde).imag = ", gfu_DtN(mesh_pt_rtilde).imag)
            #result[idx_f,idx_lami] = rho_surface_sq*gfu_DtN(mesh_pt_rtilde).imag
            result[idx_f,idx_lami] = gfu_DtN(mesh_pt_rtilde).imag
            #print("result[idx_f,idx_lami] = ", result[idx_f,idx_lami]) 
            #result[idx_f,idx_lami] = gfu_DtN(mesh_pt_rtilde).imag


def compute_power_spectrum_dirichlet(result,use_conjugate=True):
    print("Hello from compute_power_spectrum_dirichlet")
    #print("height a = ", a)
    print("mesh_pts_to_surface = ", mesh_pts_to_surface)
    mesh_1D = Make1DMesh_givenpts(mesh_pts_to_surface)
    mesh_pt_rtilde = mesh_1D(1.0)
    rho_surface_sq = sqrt(rho_cleanB(mesh_pt_rtilde))
    
    fes_DtN = H1(mesh_1D, complex=True,  order=order_ODE, dirichlet=[2])
    #fes_DtN = H1(mesh_1D, complex=True,  order=order_ODE, dirichlet=[])
    print("fes_DtN.FreeDofs() =  ", fes_DtN.FreeDofs())

    u_DtN,v_DtN = fes_DtN.TnT()
    gfu_DtN = GridFunction (fes_DtN)



    f_DtN = LinearForm(fes_DtN)
    f_DtN.Assemble()
    r_DtN = f_DtN.vec.CreateVector()

    for i in range(fes_DtN.ndof):
        gfu_DtN.vec[i] = 1.0
        f_DtN.vec[i] =  gfu_DtN(mesh_pt_rtilde)
        gfu_DtN.vec[i] = 0.0
    #print("f_DtN.vec =" , f_DtN.vec)

    for idx_f,f_hz in enumerate(f_hzs):
        print("idx_f =", idx_f)
        for idx_lami,lami in enumerate(lam_continuous):    
            omega, omega_squared = get_damping(f_hz,lami)
            #print("omega_squared = ", omega_squared)
            #pot_1D = pot_rho_B - omega_squared/pot_c_B**2
            
            a_DtN = BilinearForm (fes_DtN, symmetric=True)
            #a_DtN += SymbolicBFI((x**2)*grad(u_DtN)*grad(v_DtN)) 
            #a_DtN += SymbolicBFI(( lami.item() +  pot_1D*x**2 )*u_DtN*v_DtN)


            a_DtN += SymbolicBFI((x**2/rho_cleanB)*grad(u_DtN)*grad(v_DtN)) 
            a_DtN += SymbolicBFI(( a**2*lami.item()/rho_cleanB - omega_squared*x**2/(rho_cleanB*c_cleanB**2) )*u_DtN*v_DtN)
            #a_DtN += SymbolicBFI((x**2/rho_cleanB)*dtn_nr[idx_f,idx_lami]*u_DtN*v_DtN,definedon=mesh_1D.Boundaries("right"))   

            #a_DtN += SymbolicBFI((x**2/rho_cleanB)*grad(u_DtN)*grad(v_DtN)) 
            #a_DtN += SymbolicBFI(( lami.item()/rho_cleanB - omega_squared*x**2/(rho_cleanB*c_cleanB**2) )*u_DtN*v_DtN)
            #a_DtN += SymbolicBFI(dtn_nr[idx_f,idx_lami]*u_DtN*v_DtN,definedon=mesh_1D.Boundaries("right"))   
            #a_DtN += SymbolicBFI(x**2*dtn_nr[idx_f,idx_lami]*u_DtN*v_DtN,definedon=mesh_1D.Boundaries("right"))   
            #a_DtN += SymbolicBFI((x**2/rho_cleanB)*dtn_nr[idx_f,idx_lami]*u_DtN*v_DtN,definedon=mesh_1D.Boundaries("right"))   
            a_DtN.Assemble()

            gfu_DtN.vec[:] = 0.0
            r_DtN.data = f_DtN.vec - a_DtN.mat * gfu_DtN.vec
            gfu_DtN.vec.data += a_DtN.mat.Inverse(freedofs=fes_DtN.FreeDofs(),inverse=solver) * r_DtN
            #print("gfu_DtN(mesh_pt_rtilde).imag = ", gfu_DtN(mesh_pt_rtilde).imag)
            #result[idx_f,idx_lami] = rho_surface_sq*gfu_DtN(mesh_pt_rtilde).imag
            result[idx_f,idx_lami] = gfu_DtN(mesh_pt_rtilde).imag
            #print("result[idx_f,idx_lami] = ", result[idx_f,idx_lami]) 
            #result[idx_f,idx_lami] = gfu_DtN(mesh_pt_rtilde).imag


def compute_power_spectrum_dirichlet_ext(result,use_conjugate=True):
    print("Hello from compute_power_spectrum_dirichlet")
    #print("height a = ", a)
    print("mesh_pts_to_surface = ", mesh_pts_to_surface)
    
    #end_pt = 1.0035881999999816
    #end_pt = 1.00325
    
    #end_pt = 1.005 #power-spectrum-Atmo-DirichletBC-ext-orig-r1p005-3480km.out
    #end_pt = 1.0025 #power-spectrum-Atmo-DirichletBC-ext-origr1p0025-1740km.out
    #end_pt = 1.001437 
    end_pt = 1.001078
    dxw = mesh_pts_to_surface[-1]-mesh_pts_to_surface[-2] 
    pt_current = mesh_pts_to_surface[-1] + dxw 
    while( pt_current < end_pt): 
        mesh_pts_to_surface.append(pt_current) 
        pt_current += dxw  

    print("mesh_pts_to_surface = ", mesh_pts_to_surface)

    mesh_1D = Make1DMesh_givenpts(mesh_pts_to_surface)
    mesh_pt_rtilde = mesh_1D(1.0)
    rho_surface_sq = sqrt(rho_cleanB(mesh_pt_rtilde))
    
    fes_DtN = H1(mesh_1D, complex=True,  order=order_ODE, dirichlet=[2])
    #fes_DtN = H1(mesh_1D, complex=True,  order=order_ODE, dirichlet=[])
    print("fes_DtN.FreeDofs() =  ", fes_DtN.FreeDofs())

    u_DtN,v_DtN = fes_DtN.TnT()
    gfu_DtN = GridFunction (fes_DtN)



    f_DtN = LinearForm(fes_DtN)
    f_DtN.Assemble()
    r_DtN = f_DtN.vec.CreateVector()

    for i in range(fes_DtN.ndof):
        gfu_DtN.vec[i] = 1.0
        f_DtN.vec[i] =  gfu_DtN(mesh_pt_rtilde)
        gfu_DtN.vec[i] = 0.0
    #print("f_DtN.vec =" , f_DtN.vec)

    for idx_f,f_hz in enumerate(f_hzs):
        print("idx_f =", idx_f)
        for idx_lami,lami in enumerate(lam_continuous):    
            omega, omega_squared = get_damping(f_hz,lami)
            #print("omega_squared = ", omega_squared)
            #pot_1D = pot_rho_B - omega_squared/pot_c_B**2
            
            a_DtN = BilinearForm (fes_DtN, symmetric=True)
            #a_DtN += SymbolicBFI((x**2)*grad(u_DtN)*grad(v_DtN)) 
            #a_DtN += SymbolicBFI(( lami.item() +  pot_1D*x**2 )*u_DtN*v_DtN)


            a_DtN += SymbolicBFI((x**2/rho_cleanB)*grad(u_DtN)*grad(v_DtN)) 
            a_DtN += SymbolicBFI(( a**2*lami.item()/rho_cleanB - omega_squared*x**2/(rho_cleanB*c_cleanB**2) )*u_DtN*v_DtN)
            #a_DtN += SymbolicBFI((x**2/rho_cleanB)*dtn_nr[idx_f,idx_lami]*u_DtN*v_DtN,definedon=mesh_1D.Boundaries("right"))   

            #a_DtN += SymbolicBFI((x**2/rho_cleanB)*grad(u_DtN)*grad(v_DtN)) 
            #a_DtN += SymbolicBFI(( lami.item()/rho_cleanB - omega_squared*x**2/(rho_cleanB*c_cleanB**2) )*u_DtN*v_DtN)
            #a_DtN += SymbolicBFI(dtn_nr[idx_f,idx_lami]*u_DtN*v_DtN,definedon=mesh_1D.Boundaries("right"))   
            #a_DtN += SymbolicBFI(x**2*dtn_nr[idx_f,idx_lami]*u_DtN*v_DtN,definedon=mesh_1D.Boundaries("right"))   
            #a_DtN += SymbolicBFI((x**2/rho_cleanB)*dtn_nr[idx_f,idx_lami]*u_DtN*v_DtN,definedon=mesh_1D.Boundaries("right"))   
            a_DtN.Assemble()

            gfu_DtN.vec[:] = 0.0
            r_DtN.data = f_DtN.vec - a_DtN.mat * gfu_DtN.vec
            gfu_DtN.vec.data += a_DtN.mat.Inverse(freedofs=fes_DtN.FreeDofs(),inverse=solver) * r_DtN
            #print("gfu_DtN(mesh_pt_rtilde).imag = ", gfu_DtN(mesh_pt_rtilde).imag)
            #result[idx_f,idx_lami] = rho_surface_sq*gfu_DtN(mesh_pt_rtilde).imag
            result[idx_f,idx_lami] = gfu_DtN(mesh_pt_rtilde).imag
            #print("result[idx_f,idx_lami] = ", result[idx_f,idx_lami]) 
            #result[idx_f,idx_lami] = gfu_DtN(mesh_pt_rtilde).imag


def get_ODE_DtN_Dirichlet(dtn_nr,use_PML):
   
    outer_mesh = []
    dxw = mesh_pts_to_surface[-1]-mesh_pts_to_surface[-2] 
    pt_current = a
    while( pt_current < end_pt_Dirichlet ): 
        outer_mesh.append(pt_current) 
        pt_current += dxw  

    rho_cleanB =  rho_a * exp(-alpha_infty*(x-a))   
    c_cleanB = c_infty

    print("Hello from get_ODE_DtN_Dirichlet")
    print("outer_mesh = ", outer_mesh)
    
    mesh_1D = Make1DMesh_givenpts(outer_mesh)  
    fes_DtN = H1(mesh_1D, complex=True,  order=order_ODE, dirichlet=[1,2])
    u_DtN,v_DtN = fes_DtN.TnT()
    gfu_DtN = GridFunction (fes_DtN)

    f_DtN = LinearForm(fes_DtN)
    f_DtN.Assemble()
    r_DtN = f_DtN.vec.CreateVector()
    frees_DtN = [i for i in range(1,fes_DtN.ndof)]

    for idx_f,f_hz in enumerate(f_hzs):
        print("f_hz = ", f_hz)

                
        if use_PML:
            pml_phys = get_PML(f_hz)
            mesh_1D.SetPML(pml_phys,definedon=1)
        
        for idx_lami,lami in enumerate(lam_continuous):    
        
            omega, omega_squared = get_damping(f_hz,lami)
            #pot_1D = pot_rho_B - omega_squared/pot_c_B**2
            
            a_DtN = BilinearForm (fes_DtN, symmetric=False)
            a_DtN += SymbolicBFI((x**2/rho_cleanB)*grad(u_DtN)*grad(v_DtN)) 
            a_DtN += SymbolicBFI(( a**2*lami.item()/rho_cleanB - omega_squared*x**2/(rho_cleanB*c_cleanB**2) )*u_DtN*v_DtN)
            a_DtN.Assemble()

            gfu_DtN.vec[:] = 0.0
            gfu_DtN.Set(1.0, definedon=mesh_1D.Boundaries("left"))
            r_DtN.data = f_DtN.vec - a_DtN.mat * gfu_DtN.vec
            gfu_DtN.vec.data += a_DtN.mat.Inverse(freedofs=fes_DtN.FreeDofs(),inverse=solver) * r_DtN
           
            rows_DtN,cols_DtN,vals_DtN = a_DtN.mat.COO()
            A_DtN = csr_matrix((vals_DtN,(rows_DtN,cols_DtN)))
            val = -P_DoFs(A_DtN,[0],frees_DtN).dot(gfu_DtN.vec.FV().NumPy()[frees_DtN])-P_DoFs(A_DtN,[0],[0]).dot(gfu_DtN.vec.FV().NumPy()[0])

            dtn_nr[idx_f,idx_lami] =  -rho_a*val.item()/a**2 

        if use_PML:
            mesh_1D.UnSetPML(1)

    np.savetxt('data/ODE_dtn_nr-Dirichlet-{0}km.out'.format( km_str ),dtn_nr,delimiter=',')



dtn_nr = np.zeros((len(f_hzs),len(L_range)),dtype="complex")
#loaded_dtn_nr = np.zeros((len(f_hzs),len(L_range)),dtype="complex")
power_spectrum_dtn = np.zeros((len(f_hzs),len(L_range)))
power_spectrum = np.zeros((len(f_hzs),len(L_range)))


if atmospheric_model == "VALC": 
    if dtn_approx == "exact":
        with TaskManager():
            get_ODE_DtN(dtn_nr,use_PML)
            #dtn_nr = np.loadtxt('ODE_dtn_nr.out',dtype="complex",delimiter=',')
            compute_power_spectrum_dtn(power_spectrum, dtn_nr )
            np.savetxt('data/Greens-fct-imag-VALC.out',power_spectrum,delimiter=',')
    if dtn_approx == "learned":
        try: 
            dtn_nr = np.loadtxt('data/ODE_dtn_nr.out',dtype="complex",delimiter=',')
        except:
            get_ODE_DtN(dtn_nr,use_PML)
        learned_dtn_nrs = computed_learned_dtn(dtn_nr,Nrs)
        for N in Nrs:
            dtn_nr_learned = np.loadtxt('data/dtn_nr_learned_N{0}-VALC.out'.format(N),dtype="complex",delimiter=',')
            with TaskManager():
                compute_power_spectrum_dtn(power_spectrum, dtn_nr_learned )
            np.savetxt('data/Greens-fct-imag-N{0}-VALC.out'.format(N),power_spectrum,delimiter=',')
else:
    if dtn_approx == "exact":
        #try:
        #    dtn_nr = np.loadtxt('ODE_dtn_nr-aHeight{0}.out'.format(a_str_list[idx_a]),dtype="complex",delimiter=',')
        #    print("Loaded dtn numbers!")
        #except:
        try:
            dtn_nr_whitw = np.loadtxt('data/dtn_nr_Whitw-comp-orig.out',dtype="complex",delimiter=',')
        except:
            dtn_nr_whitw = np.zeros((len(f_hzs),len(L_range)),dtype="complex")
            get_atmo_dtn(dtn_nr_whitw)
            #dtn_nr = dtn_nr_whitw
            get_ODE_DtN(dtn_nr,dtn_nr_whitw,use_PML)
        with TaskManager():
            compute_power_spectrum_dtn(power_spectrum, dtn_nr_whitw)
        np.savetxt('data/Greens-fct-imag-exact.out',power_spectrum,delimiter=',')
        #np.savetxt('power-spectrum-nonlocal.out',power_spectrum,delimiter=',')
    if dtn_approx == "Dirichlet":
        with TaskManager():
            compute_power_spectrum_dirichlet(power_spectrum)
        #np.savetxt('power-spectrum-Atmo-DirichletBC-aHeight{0}-orig.out'.format(a_str_list[idx_a]),power_spectrum,delimiter=',')
        np.savetxt('data/Greens-fct-imag-Dirichlet-bc.out',power_spectrum,delimiter=',')
        #np.savetxt('power-spectrum-Atmo-NeumannBC-aHeight{0}-orig.out'.format(a_str_list[idx_a]),power_spectrum,delimiter=',') 
    if dtn_approx == "Dirichlet-ext":
        with TaskManager():
            compute_power_spectrum_dirichlet_ext(power_spectrum)
        np.savetxt('data/power-spectrum-Atmo-DirichletBC-ext-orig.out',power_spectrum,delimiter=',')
    if dtn_approx == "Dirichlet-dtn":
        dtn_nr_Dirichlet = np.zeros((len(f_hzs),len(L_range)),dtype="complex")
        get_ODE_DtN_Dirichlet( dtn_nr_Dirichlet,False)
        compute_power_spectrum_dtn(power_spectrum, dtn_nr_Dirichlet )
        np.savetxt('data/power-spectrum-Dirichlet-{0}-dtn-orig.out'.format(km_str),power_spectrum,delimiter=',')
    if dtn_approx == "nonlocal":
        get_atmo_nonlocal(dtn_nr)
        dtn_nr = np.loadtxt('data/dtn_nr_nonlocal.out',dtype="complex",delimiter=',')
        with TaskManager():
            compute_power_spectrum_dtn(power_spectrum, dtn_nr )
        np.savetxt('data/Greens-fct-imag-nonlocal.out',power_spectrum,delimiter=',')
    if dtn_approx == "SHF1a":
        get_atmo_SHF1a(dtn_nr)
        #dtn_nr = np.loadtxt('dtn_nr_S-HF-1a.out',dtype="complex",delimiter=',') 
        with TaskManager():
            compute_power_spectrum_dtn(power_spectrum, dtn_nr )
        np.savetxt('data/Greens-fct-imag-SHF1a.out',power_spectrum,delimiter=',') 
    if dtn_approx == "SAI0":
        get_atmo_SAI0(dtn_nr)
        #get_atmo_SHF1a(dtn_nr)
        dtn_nr = np.loadtxt('data/dtn_nr_SAI-0.out',dtype="complex",delimiter=',') 
        with TaskManager():
            compute_power_spectrum_dtn(power_spectrum, dtn_nr )
        np.savetxt('data/power-spectrum-SAI-0.out',power_spectrum,delimiter=',')
    if dtn_approx == "SAI1":
        get_atmo_SAI1(dtn_nr)
        #dtn_nr = np.loadtxt('dtn_nr_SAI-1.out',dtype="complex",delimiter=',') 
        with TaskManager():
            compute_power_spectrum_dtn(power_spectrum, dtn_nr )
        np.savetxt('data/Greens-fct-imag-SAI1.out',power_spectrum,delimiter=',')
    if dtn_approx == "AHF1":
        get_atmo_AHF1(dtn_nr)
        dtn_nr = np.loadtxt('data/dtn_nr_AHF1.out',dtype="complex",delimiter=',') 
        with TaskManager():
            compute_power_spectrum_dtn(power_spectrum, dtn_nr )
        np.savetxt('data/power-spectrum-AHF1.out',power_spectrum,delimiter=',')
    if dtn_approx == "ARBC1":
        get_atmo_ARBC1(dtn_nr)
        dtn_nr = np.loadtxt('data/dtn_nr_ARBC1.out',dtype="complex",delimiter=',') 
        with TaskManager():
            compute_power_spectrum_dtn(power_spectrum, dtn_nr )
        np.savetxt('data/power-spectrum-ARBC1.out',power_spectrum,delimiter=',')
    
    if dtn_approx == "only_learning":
        try:
            dtn_nr = np.loadtxt('data/ODE_dtn_nr-aHeight{0}.out'.format(a_str_list[idx_a]),dtype="complex",delimiter=',')
        except:
            try:
                dtn_nr_whitw = np.loadtxt('data/dtn_nr_Whitw-comp.out',dtype="complex",delimiter=',')
            except:
                dtn_nr_whitw = np.zeros((len(f_hzs),len(L_range)),dtype="complex")
                get_atmo_dtn(dtn_nr_whitw)
            get_ODE_DtN(dtn_nr,dtn_nr_whitw,use_PML)
        learned_dtn_nrs = computed_learned_dtn(dtn_nr,Nrs)
    if dtn_approx == "learned":
        #try:
        #    dtn_nr = np.loadtxt('dtn_nr_Whitw-comp.out',dtype="complex",delimiter=',')
        #except:
        #    get_atmo_dtn(dtn_nr)
        #learned_dtn_nrs = computed_learned_dtn(dtn_nr,Nrs)
        #for N in [0]:
        #for N in Nrs:
        for N in [N_compute]:
            try:
                dtn_nr_learned = np.loadtxt('data/dtn_nr_learned_N{0}-Atmo-aHeight{1}.out'.format(N,a_str_list[idx_a]),dtype="complex",delimiter=',')
            except:
                raise ValueError('File dtn_nr_learned_N{0}-Atmo-aHeight{1}.out not found!'.format(N,a_str_list[idx_a]))
                #try:
                #    dtn_nr = np.loadtxt('dtn_nr_Whitw-comp.out',dtype="complex",delimiter=',')
                #except:
                #    get_atmo_dtn(dtn_nr)
                #learned_dtn_nrs = computed_learned_dtn(dtn_nr,Nrs)
                #dtn_nr_learned = np.loadtxt('dtn_nr_learned_N{0}-Atmo.out'.format(N),dtype="complex",delimiter=',') 
            with TaskManager():
                compute_power_spectrum_dtn(power_spectrum, dtn_nr_learned )
            np.savetxt('data/Greens-fct-imag-N{0}.out'.format(N),power_spectrum,delimiter=',')
    
