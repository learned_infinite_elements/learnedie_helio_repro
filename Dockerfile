FROM ngsxfem/ngsxfem:latest

USER root
USER root
RUN sed -i -re 's/([a-z]{2}\.)?archive.ubuntu.com|security.ubuntu.com/old-releases.ubuntu.com/g' /etc/apt/sources.list
RUN apt-get update && apt-get dist-upgrade -y
RUN apt-get install -y \
  libgoogle-glog-dev \
  libatlas-base-dev \
  libeigen3-dev \
  libsuitesparse-dev \
  libmpfr-dev \
  libgmp-dev \
  libflint-dev \
  libflint-arb-dev \
  libflint-arb2 \
  wget

RUN pip3 install psutil mpmath
RUN pip3 install --upgrade numpy scipy matplotlib
        
WORKDIR /home/app
RUN wget http://ceres-solver.org/ceres-solver-1.14.0.tar.gz
RUN tar zxf ceres-solver-1.14.0.tar.gz
RUN mkdir /home/app/ceres-bin
WORKDIR /home/app/ceres-bin
RUN cmake ../ceres-solver-1.14.0
RUN make -j3
RUN make install

WORKDIR /home/app        
RUN git clone https://gitlab.gwdg.de/learned_infinite_elements/learnedie_helio_repro.git
WORKDIR /home/app/learnedie_helio_repro
RUN git pull origin main
RUN git submodule update --init --recursive  
WORKDIR /home/app/learnedie_helio_repro/ceres_dtn
RUN python3 setup.py install --user
WORKDIR /home/app/learnedie_helio_repro/ngs_arb
RUN python3 setup.py install --user
        
USER root
RUN chown -R ${NB_UID} ${HOME}
USER ${NB_USER}
        
WORKDIR /home/app/learnedie_helio_repro/reproduction
